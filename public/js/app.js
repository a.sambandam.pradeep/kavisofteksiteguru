/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function (t) {
  function e(e) {
    for (var s, r, o = e[0], l = e[1], c = e[2], m = 0, u = []; m < o.length; m++) {
      r = o[m], Object.prototype.hasOwnProperty.call(i, r) && i[r] && u.push(i[r][0]), i[r] = 0;
    }

    for (s in l) {
      Object.prototype.hasOwnProperty.call(l, s) && (t[s] = l[s]);
    }

    d && d(e);

    while (u.length) {
      u.shift()();
    }

    return n.push.apply(n, c || []), a();
  }

  function a() {
    for (var t, e = 0; e < n.length; e++) {
      for (var a = n[e], s = !0, o = 1; o < a.length; o++) {
        var l = a[o];
        0 !== i[l] && (s = !1);
      }

      s && (n.splice(e--, 1), t = r(r.s = a[0]));
    }

    return t;
  }

  var s = {},
      i = {
    app: 0
  },
      n = [];

  function r(e) {
    if (s[e]) return s[e].exports;
    var a = s[e] = {
      i: e,
      l: !1,
      exports: {}
    };
    return t[e].call(a.exports, a, a.exports, r), a.l = !0, a.exports;
  }

  r.m = t, r.c = s, r.d = function (t, e, a) {
    r.o(t, e) || Object.defineProperty(t, e, {
      enumerable: !0,
      get: a
    });
  }, r.r = function (t) {
    "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
      value: "Module"
    }), Object.defineProperty(t, "__esModule", {
      value: !0
    });
  }, r.t = function (t, e) {
    if (1 & e && (t = r(t)), 8 & e) return t;
    if (4 & e && "object" === _typeof(t) && t && t.__esModule) return t;
    var a = Object.create(null);
    if (r.r(a), Object.defineProperty(a, "default", {
      enumerable: !0,
      value: t
    }), 2 & e && "string" != typeof t) for (var s in t) {
      r.d(a, s, function (e) {
        return t[e];
      }.bind(null, s));
    }
    return a;
  }, r.n = function (t) {
    var e = t && t.__esModule ? function () {
      return t["default"];
    } : function () {
      return t;
    };
    return r.d(e, "a", e), e;
  }, r.o = function (t, e) {
    return Object.prototype.hasOwnProperty.call(t, e);
  }, r.p = "/";
  var o = window["webpackJsonp"] = window["webpackJsonp"] || [],
      l = o.push.bind(o);
  o.push = e, o = o.slice();

  for (var c = 0; c < o.length; c++) {
    e(o[c]);
  }

  var d = l;
  n.push([0, "chunk-vendors"]), a();
})({
  0: function _(t, e, a) {
    t.exports = a("56d7");
  },
  "0a95": function a95(t, e, a) {},
  "0efd": function efd(t, e, a) {
    "use strict";

    var s = a("cd1f"),
        i = a.n(s);
    i.a;
  },
  1423: function _(t, e, a) {},
  "251d": function d(t, e, a) {
    "use strict";

    var s = a("553d"),
        i = a.n(s);
    i.a;
  },
  "338e": function e(t, _e2, a) {},
  "39eb": function eb(t, e, a) {
    "use strict";

    var s = a("442c"),
        i = a.n(s);
    i.a;
  },
  "3f6f": function f6f(t, e, a) {
    "use strict";

    var s = a("1423"),
        i = a.n(s);
    i.a;
  },
  "442c": function c(t, e, a) {},
  "52d0": function d0(t, e, a) {},
  "553d": function d(t, e, a) {},
  "56d7": function d7(t, e, a) {
    "use strict";

    a.r(e);
    a("e260"), a("e6cf"), a("cca6"), a("a79d");

    var s = a("2b0e"),
        i = a("bc3a"),
        n = a.n(i),
        r = function r() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("router-view");
    },
        o = [],
        l = {},
        c = l,
        d = a("2877"),
        m = Object(d["a"])(c, r, o, !1, null, null, null),
        u = m.exports,
        p = a("ba48"),
        h = a.n(p),
        f = a("a7fe"),
        v = a.n(f),
        b = (a("c740"), a("4160"), a("a434"), a("159b"), function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "notifications"
      }, [a("transition-group", {
        attrs: {
          name: "list",
          mode: "in-out"
        }
      }, t._l(t.notifications, function (e) {
        return a("notification", {
          key: e.timestamp.getTime(),
          attrs: {
            message: e.message,
            icon: e.icon,
            type: e.type,
            timeout: e.timeout,
            timestamp: e.timestamp,
            "vertical-align": e.verticalAlign,
            "horizontal-align": e.horizontalAlign
          },
          on: {
            "on-close": t.removeNotification
          }
        });
      }), 1)], 1);
    }),
        g = [],
        _ = function _() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "alert open alert-with-icon",
        "class": [t.verticalAlign, t.horizontalAlign, t.alertType],
        style: t.customPosition,
        attrs: {
          "data-notify": "container",
          role: "alert",
          "data-notify-position": "top-center"
        },
        on: {
          click: function click(e) {
            return t.close();
          }
        }
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true",
          "data-notify": "dismiss"
        },
        on: {
          click: t.close
        }
      }, [t._v(" × ")]), t.icon ? a("i", {
        staticClass: "material-icons",
        attrs: {
          "data-notify": "icon"
        }
      }, [t._v(t._s(t.icon))]) : t._e(), a("span", {
        attrs: {
          "data-notify": "message"
        },
        domProps: {
          innerHTML: t._s(t.message)
        }
      })]);
    },
        C = [],
        y = (a("4de4"), a("a9e3"), {
      name: "notification",
      props: {
        message: String,
        icon: String,
        verticalAlign: {
          type: String,
          "default": "top"
        },
        horizontalAlign: {
          type: String,
          "default": "center"
        },
        type: {
          type: String,
          "default": "info"
        },
        timeout: {
          type: Number,
          "default": 10500
        },
        timestamp: {
          type: Date,
          "default": function _default() {
            return new Date();
          }
        }
      },
      data: function data() {
        return {
          elmHeight: 0
        };
      },
      computed: {
        hasIcon: function hasIcon() {
          return this.icon && this.icon.length > 0;
        },
        alertType: function alertType() {
          return "alert-".concat(this.type);
        },
        customPosition: function customPosition() {
          var t = this,
              e = 20,
              a = this.elmHeight + 10,
              s = this.$notifications.state.filter(function (e) {
            return e.horizontalAlign === t.horizontalAlign && e.verticalAlign === t.verticalAlign && e.timestamp <= t.timestamp;
          }).length,
              i = (s - 1) * a + e,
              n = {};
          return "top" === this.verticalAlign ? n.top = "".concat(i, "px") : n.bottom = "".concat(i, "px"), n;
        }
      },
      methods: {
        close: function close() {
          this.$emit("on-close", this.timestamp);
        }
      },
      mounted: function mounted() {
        this.elmHeight = this.$el.clientHeight, this.timeout && setTimeout(this.close, this.timeout);
      }
    }),
        w = y,
        k = (a("8075"), Object(d["a"])(w, _, C, !1, null, "2a855839", null)),
        x = k.exports,
        S = {
      components: {
        Notification: x
      },
      data: function data() {
        return {
          notifications: this.$notifications.state
        };
      },
      methods: {
        removeNotification: function removeNotification(t) {
          this.$notifications.removeNotification(t);
        }
      }
    },
        $ = S,
        T = (a("6ae5"), Object(d["a"])($, b, g, !1, null, null, null)),
        P = T.exports,
        j = {
      state: [],
      removeNotification: function removeNotification(t) {
        var e = this.state.findIndex(function (e) {
          return e.timestamp === t;
        });
        -1 !== e && this.state.splice(e, 1);
      },
      addNotification: function addNotification(t) {
        t.timestamp = new Date(), t.timestamp.setMilliseconds(t.timestamp.getMilliseconds() + this.state.length), this.state.push(t);
      },
      notify: function notify(t) {
        var e = this;
        Array.isArray(t) ? t.forEach(function (t) {
          e.addNotification(t);
        }) : this.addNotification(t);
      }
    },
        O = {
      install: function install(t) {
        t.mixin({
          data: function data() {
            return {
              notificationStore: j
            };
          },
          methods: {
            notify: function notify(t) {
              this.notificationStore.notify(t);
            }
          }
        }), Object.defineProperty(t.prototype, "$notify", {
          get: function get() {
            return this.$root.notify;
          }
        }), Object.defineProperty(t.prototype, "$notifications", {
          get: function get() {
            return this.$root.notificationStore;
          }
        }), t.component("Notifications", P);
      }
    },
        I = O,
        z = function z() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        directives: [{
          name: "click-outside",
          rawName: "v-click-outside",
          value: t.closeDropDown,
          expression: "closeDropDown"
        }],
        "class": [{
          open: t.isOpen
        }, {
          dropdown: "down" === t.direction
        }, {
          dropup: "up" === t.direction
        }],
        on: {
          click: t.toggleDropDown
        }
      }, [t._t("title", [a("a", {
        staticClass: "dropdown-toggle",
        attrs: {
          "data-toggle": "dropdown",
          href: "javascript:void(0)"
        }
      }, [a("i", {
        "class": t.icon
      }), a("p", {
        staticClass: "notification"
      }, [t._v(" " + t._s(t.title) + " "), a("b", {
        staticClass: "caret"
      })])])]), t._t("default")], 2);
    },
        E = [],
        A = {
      name: "drop-down",
      props: {
        direction: {
          type: String,
          "default": "down"
        },
        multiLevel: {
          type: Boolean,
          "default": !1
        },
        title: String,
        icon: String
      },
      data: function data() {
        return {
          isOpen: !1
        };
      },
      methods: {
        toggleDropDown: function toggleDropDown() {
          this.multiLevel ? this.isOpen = !0 : this.isOpen = !this.isOpen;
        },
        closeDropDown: function closeDropDown() {
          this.isOpen = !1;
        }
      }
    },
        D = A,
        M = Object(d["a"])(D, z, E, !1, null, null, null),
        L = M.exports,
        B = a("7bb1"),
        N = a("7c76"),
        R = {
      install: function install(t) {
        t.component("drop-down", L), t.component("ValidationProvider", B["b"]), t.component("ValidationObserver", B["a"]), t.component("CollapseTransition", N["a"]), t.component("SlideYDownTransition", N["c"]), t.component("FadeTransition", N["b"]), t.component("ZoomCenterTransition", N["d"]);
      }
    },
        V = R,
        U = a("c7db"),
        H = {
      install: function install(t) {
        t.directive("click-outside", U["directive"]);
      }
    },
        F = H,
        K = (a("c975"), function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "sidebar",
        style: t.sidebarStyle,
        attrs: {
          "data-color": t.activeColor,
          "data-image": t.backgroundImage,
          "data-background-color": t.backgroundColor
        }
      }, [a("div", {
        staticClass: "logo pa-2"
      }, [a("a", {
        staticClass: "simple-text logo-normal",
        attrs: {
          href: "https://kaviInternals.local",
          target: "_blank"
        }
      }, [[t._v(t._s(t.title))]], 2)]), a("div", {
        ref: "sidebarScrollArea",
        staticClass: "sidebar-wrapper"
      }, [t._t("default"), a("md-list", {
        staticClass: "nav"
      }, [t._t("links", t._l(t.sidebarLinks, function (e, s) {
        return a("sidebar-item", {
          key: e.name + s,
          attrs: {
            link: e
          }
        }, t._l(e.children, function (t, e) {
          return a("sidebar-item", {
            key: t.name + e,
            attrs: {
              link: t
            }
          });
        }), 1);
      }))], 2)], 2)]);
    }),
        W = [],
        Y = {
      name: "sidebar",
      props: {
        title: {
          type: String,
          "default": "Kavi Internals"
        },
        activeColor: {
          type: String,
          "default": "green",
          validator: function validator(t) {
            var e = ["", "purple", "azure", "green", "orange", "danger", "rose"];
            return -1 !== e.indexOf(t);
          }
        },
        backgroundImage: {
          type: String,
          "default": "".concat("http://localhost:8080/", "/img/sidebar-2.jpg")
        },
        backgroundColor: {
          type: String,
          "default": "black",
          validator: function validator(t) {
            var e = ["", "black", "white", "red"];
            return -1 !== e.indexOf(t);
          }
        },
        logo: {
          type: String,
          "default": "".concat("http://localhost:8080/", "/img/vue-logo.png")
        },
        sidebarLinks: {
          type: Array,
          "default": function _default() {
            return [];
          }
        },
        autoClose: {
          type: Boolean,
          "default": !0
        }
      },
      created: function created() {
        this.$sidebar.toggleMinimize();
      },
      provide: function provide() {
        return {
          autoClose: this.autoClose
        };
      },
      methods: {
        minimizeSidebar: function minimizeSidebar() {
          this.$sidebar && this.$sidebar.toggleMinimize();
        }
      },
      computed: {
        sidebarStyle: function sidebarStyle() {
          return {
            backgroundImage: "url(".concat(this.backgroundImage, ")")
          };
        }
      },
      beforeDestroy: function beforeDestroy() {
        this.$sidebar.showSidebar && (this.$sidebar.showSidebar = !1);
      }
    },
        q = Y,
        G = (a("0efd"), Object(d["a"])(q, K, W, !1, null, null, null)),
        J = G.exports,
        X = function X() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a(t.baseComponent, {
        tag: "component",
        "class": {
          active: t.isActive
        },
        attrs: {
          to: t.link.path ? t.link.path : "/",
          tag: "li"
        }
      }, [t.isMenu ? a("a", {
        staticClass: "nav-link sidebar-menu-item",
        attrs: {
          href: "#",
          "aria-expanded": !t.collapsed,
          "data-toggle": "collapse"
        },
        on: {
          click: function click(e) {
            return e.preventDefault(), t.collapseMenu(e);
          }
        }
      }, [t.link.icon ? a("md-icon", [t._v(t._s(t.link.icon))]) : t._e(), t.link.image ? a("md-icon", [a("md-avatar", {
        staticStyle: {
          height: "auto",
          width: "auto",
          "min-width": "auto"
        }
      }, [a("img", {
        attrs: {
          src: t.link.image,
          alt: "Avatar"
        }
      })])], 1) : t._e(), a("p", [t._v(" " + t._s(t.link.name) + " "), a("b", {
        staticClass: "caret"
      })])], 1) : t._e(), a("collapse-transition", [t.$slots["default"] || this.isMenu ? a("div", {
        directives: [{
          name: "show",
          rawName: "v-show",
          value: !t.collapsed,
          expression: "!collapsed"
        }]
      }, [a("ul", {
        staticClass: "nav"
      }, [t._t("default")], 2)]) : t._e()]), 0 === t.children.length && !t.$slots["default"] && t.link.path ? t._t("title", [a(t.elementType(t.link, !1), {
        tag: "component",
        staticClass: "nav-link",
        "class": {
          active: t.link.active
        },
        attrs: {
          to: t.link.path,
          target: t.link.target,
          href: t.link.path
        },
        nativeOn: {
          click: function click(e) {
            return t.linkClick(e);
          }
        }
      }, [t.addLink ? [a("span", {
        staticClass: "sidebar-mini"
      }, [t._v(t._s(t.linkPrefix))]), a("span", {
        staticClass: "sidebar-normal"
      }, [t._v(t._s(t.link.name))])] : [a("md-icon", [t._v(t._s(t.link.icon))]), a("p", [t._v(t._s(t.link.name))])]], 2)]) : t._e()], 2);
    },
        Q = [],
        Z = (a("7db0"), a("a15b"), a("d81d"), a("b0c0"), a("ac1f"), a("466d"), a("1276"), a("2ca0"), a("9911"), {
      name: "sidebar-item",
      props: {
        menu: {
          type: Boolean,
          "default": !1
        },
        opened: {
          type: Boolean,
          "default": !1
        },
        link: {
          type: Object,
          "default": function _default() {
            return {
              name: "",
              icon: "",
              image: "",
              path: "",
              children: []
            };
          }
        }
      },
      provide: function provide() {
        return {
          addLink: this.addChild,
          removeLink: this.removeChild
        };
      },
      inject: {
        addLink: {
          "default": null
        },
        removeLink: {
          "default": null
        },
        autoClose: {
          "default": !0
        }
      },
      data: function data() {
        return {
          children: [],
          collapsed: !this.opened
        };
      },
      computed: {
        baseComponent: function baseComponent() {
          return this.isMenu || this.link.isRoute ? "li" : "router-link";
        },
        linkPrefix: function linkPrefix() {
          if (this.link.name) {
            var t = this.link.name.split(" ");
            return t.map(function (t) {
              return t.substring(0, 1);
            }).join("");
          }

          return !1;
        },
        isMenu: function isMenu() {
          return this.children.length > 0 || !0 === this.menu;
        },
        isActive: function isActive() {
          var t = this;

          if (this.$route && this.$route.path) {
            var e = this.children.find(function (e) {
              return t.$route.path.startsWith(e.link.path);
            });
            if (void 0 !== e) return !0;
          }

          return !1;
        }
      },
      methods: {
        addChild: function addChild(t) {
          var e = this.$slots["default"].indexOf(t.$vnode);
          this.children.splice(e, 0, t);
        },
        removeChild: function removeChild(t) {
          var e = this.children,
              a = e.indexOf(t);
          e.splice(a, 1);
        },
        elementType: function elementType(t) {
          var e = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
          return !1 === t.isRoute ? e ? "li" : "a" : "router-link";
        },
        linkAbbreviation: function linkAbbreviation(t) {
          var e = t.match(/\b(\w)/g);
          return e.join("");
        },
        linkClick: function linkClick() {
          this.addLink || this.$sidebar.collapseAllMenus(), this.autoClose && this.$sidebar && !0 === this.$sidebar.showSidebar && this.$sidebar.displaySidebar(!1);
        },
        collapseMenu: function collapseMenu() {
          this.collapsed && (this.$sidebar.addSidebarLink(this), this.$sidebar.collapseAllMenus()), this.collapsed = !this.collapsed;
        },
        collapseSubMenu: function collapseSubMenu(t) {
          t.collapsed = !t.collapsed;
        }
      },
      mounted: function mounted() {
        this.addLink && this.addLink(this), void 0 !== this.link.collapsed && (this.collapsed = this.link.collapsed), this.isActive && this.isMenu && (this.collapsed = !1);
      },
      destroyed: function destroyed() {
        this.$sidebar.removeSidebarLink(this), this.$el && this.$el.parentNode && this.$el.parentNode.removeChild(this.$el), this.removeLink && this.removeLink(this);
      }
    }),
        tt = Z,
        et = (a("251d"), Object(d["a"])(tt, X, Q, !1, null, null, null)),
        at = et.exports,
        st = {
      showSidebar: !1,
      sidebarLinks: [],
      linksStore: [],
      isMinimized: !1,
      displaySidebar: function displaySidebar(t) {
        this.showSidebar = t;
      },
      toggleMinimize: function toggleMinimize() {
        document.body.classList.toggle("sidebar-mini");
        var t = setInterval(function () {
          window.dispatchEvent(new Event("resize"));
        }, 180);
        setTimeout(function () {
          clearInterval(t);
        }, 1e3), this.isMinimized = !this.isMinimized;
      },
      addSidebarLink: function addSidebarLink(t) {
        this.linksStore.push(t);
      },
      removeSidebarLink: function removeSidebarLink(t) {
        var e = this.sidebarLinks.indexOf(this);
        this.linksStore.splice(e, 1);
      },
      collapseAllMenus: function collapseAllMenus() {
        this.linksStore.forEach(function (t) {
          t.collapsed = !0;
        });
      }
    },
        it = {
      install: function install(t, e) {
        e && e.sidebarLinks && (st.sidebarLinks = e.sidebarLinks), t.mixin({
          data: function data() {
            return {
              sidebarStore: st
            };
          }
        }), Object.defineProperty(t.prototype, "$sidebar", {
          get: function get() {
            return this.$root.sidebarStore;
          }
        }), t.component("side-bar", J), t.component("sidebar-item", at);
      }
    },
        nt = it,
        rt = a("43f9"),
        ot = a.n(rt),
        lt = (a("51de"), a("9e17"), a("c6e3"), a("54ba"), {
      install: function install(t) {
        t.use(V), t.use(F), t.use(ot.a), t.use(nt), t.use(I);
      }
    }),
        ct = a("5530"),
        dt = a("8c4f"),
        mt = function mt() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "wrapper",
        "class": [{
          "nav-open": t.$sidebar.showSidebar
        }, {
          rtl: t.$route.meta.rtlActive
        }]
      }, [a("notifications"), a("side-bar", {
        attrs: {
          "active-color": t.sidebarBackground,
          "background-image": t.sidebarBackgroundImage,
          "data-background-color": t.sidebarBackgroundColor
        }
      }, [a("user-menu"), a("mobile-menu"), a("template", {
        slot: "links"
      }, [t.$route.meta.rtlActive ? a("sidebar-item", {
        attrs: {
          link: {
            name: "لوحة القيادةة",
            icon: "dashboard",
            path: "/dashboard"
          }
        }
      }) : a("sidebar-item", {
        attrs: {
          link: {
            name: "Dashboard",
            icon: "dashboard",
            path: "/dashboard"
          }
        }
      }), a("sidebar-item", {
        attrs: {
          opened: "",
          link: {
            name: "Examples (API)",
            image: t.image
          }
        }
      }, [a("sidebar-item", {
        attrs: {
          link: {
            name: "User Profile",
            path: "/examples/user-profile"
          }
        }
      }), a("sidebar-item", {
        attrs: {
          link: {
            name: "User Management",
            path: "/examples/user-management/list-users"
          }
        }
      })], 1), t.$route.meta.rtlActive ? a("sidebar-item", {
        attrs: {
          link: {
            name: "الجداول",
            icon: "content_paste",
            path: "/components/table"
          }
        }
      }) : t._e(), a("sidebar-item", {
        attrs: {
          link: {
            name: "Table Lists",
            icon: "content_paste",
            path: "/components/table"
          }
        }
      }), t.$route.meta.rtlActive ? a("sidebar-item", {
        attrs: {
          link: {
            name: "طباعة",
            icon: "library_books",
            path: "/components/typography"
          }
        }
      }) : a("sidebar-item", {
        attrs: {
          link: {
            name: "Typography",
            icon: "library_books",
            path: "/components/typography"
          }
        }
      }), t.$route.meta.rtlActive ? a("sidebar-item", {
        attrs: {
          link: {
            name: "الرموز",
            icon: "bubble_chart",
            path: "/components/icons"
          }
        }
      }) : a("sidebar-item", {
        attrs: {
          link: {
            name: "Icons",
            icon: "bubble_chart",
            path: "/components/icons"
          }
        }
      }), t.$route.meta.rtlActive ? a("sidebar-item", {
        attrs: {
          link: {
            name: "خرائط جوجل",
            icon: "place",
            path: "/components/maps"
          }
        }
      }) : a("sidebar-item", {
        attrs: {
          link: {
            name: "Maps",
            icon: "place",
            path: "/components/maps"
          }
        }
      }), t.$route.meta.rtlActive ? a("sidebar-item", {
        attrs: {
          link: {
            name: "إخطارات",
            icon: "notifications",
            path: "/components/notifications"
          }
        }
      }) : a("sidebar-item", {
        attrs: {
          link: {
            name: "Notifications",
            icon: "notifications",
            path: "/components/notifications"
          }
        }
      }), t.$route.meta.rtlActive ? a("sidebar-item", {
        attrs: {
          link: {
            name: "دعم رتل",
            icon: "language",
            path: "/components/rtl"
          }
        }
      }) : a("sidebar-item", {
        attrs: {
          link: {
            name: "RTL Support",
            icon: "language",
            path: "/components/rtl"
          }
        }
      }), a("li", {
        staticClass: "button-container"
      }, [a("div", {}, [a("md-button", {
        staticClass: "md-block md-danger",
        attrs: {
          href: t.upgradeUrl,
          target: "_blank"
        }
      }, [t._v("Upgrade to PRO ")])], 1)])], 1)], 2), a("div", {
        staticClass: "main-panel"
      }, [a("top-navbar"), a("fixed-plugin", {
        attrs: {
          color: t.sidebarBackground,
          colorBg: t.sidebarBackgroundColor,
          sidebarMini: t.sidebarMini,
          sidebarImg: t.sidebarImg,
          image: t.sidebarBackgroundImage
        },
        on: {
          "update:color": function updateColor(e) {
            t.sidebarBackground = e;
          },
          "update:colorBg": function updateColorBg(e) {
            t.sidebarBackgroundColor = e;
          },
          "update:color-bg": function updateColorBg(e) {
            t.sidebarBackgroundColor = e;
          },
          "update:sidebarMini": function updateSidebarMini(e) {
            t.sidebarMini = e;
          },
          "update:sidebar-mini": function updateSidebarMini(e) {
            t.sidebarMini = e;
          },
          "update:sidebarImg": function updateSidebarImg(e) {
            t.sidebarImg = e;
          },
          "update:sidebar-img": function updateSidebarImg(e) {
            t.sidebarImg = e;
          },
          "update:image": function updateImage(e) {
            t.sidebarBackgroundImage = e;
          }
        }
      }), a("div", {
        "class": {
          content: !t.$route.meta.hideContent
        }
      }, [a("zoom-center-transition", {
        attrs: {
          duration: 200,
          mode: "out-in"
        }
      }, [a("router-view")], 1)], 1), t.$route.meta.hideFooter ? t._e() : a("content-footer")], 1)], 1);
    },
        ut = [],
        pt = a("b7f5"),
        ht = (a("7da8"), function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("md-toolbar", {
        staticClass: "md-transparent",
        "class": {
          "md-toolbar-absolute md-white md-fixed-top": t.$route.meta.navbarAbsolute
        },
        attrs: {
          "md-elevation": "0"
        }
      }, [a("div", {
        staticClass: "md-toolbar-row"
      }, [a("div", {
        staticClass: "md-toolbar-section-start"
      }, [a("h3", {
        staticClass: "md-title"
      }, [t._v(t._s(t.$route.name))])]), a("div", {
        staticClass: "md-toolbar-section-end"
      }, [a("md-button", {
        staticClass: "md-just-icon md-round md-simple md-toolbar-toggle",
        "class": {
          toggled: t.$sidebar.showSidebar
        },
        on: {
          click: t.toggleSidebar
        }
      }, [a("span", {
        staticClass: "icon-bar"
      }), a("span", {
        staticClass: "icon-bar"
      }), a("span", {
        staticClass: "icon-bar"
      })]), a("div", {
        staticClass: "md-collapse"
      }, [a("div", {
        staticClass: "md-autocomplete"
      }, [a("md-autocomplete", {
        staticClass: "search",
        attrs: {
          "md-options": t.employees,
          "md-open-on-focus": !1
        },
        model: {
          value: t.selectedEmployee,
          callback: function callback(e) {
            t.selectedEmployee = e;
          },
          expression: "selectedEmployee"
        }
      }, [t.$route.meta.rtlActive ? a("label", [t._v("بحث...")]) : a("label", [t._v("Search...")])])], 1), a("md-list", [a("md-list-item", {
        attrs: {
          href: "#/"
        }
      }, [a("i", {
        staticClass: "material-icons"
      }, [t._v("dashboard")]), a("p", {
        staticClass: "hidden-lg hidden-md"
      }, [t._v("Dashboard")])]), a("li", {
        staticClass: "md-list-item"
      }, [a("a", {
        staticClass: "md-list-item-router md-list-item-container md-button-clean dropdown",
        on: {
          click: t.goToNotifications
        }
      }, [a("div", {
        staticClass: "md-list-item-content"
      }, [a("drop-down", {
        attrs: {
          direction: "down"
        }
      }, [a("md-button", {
        staticClass: "md-button md-just-icon md-simple",
        attrs: {
          slot: "title",
          "data-toggle": "dropdown"
        },
        slot: "title"
      }, [a("md-icon", [t._v("notifications")]), a("span", {
        staticClass: "notification"
      }, [t._v("5")]), a("p", {
        staticClass: "hidden-lg hidden-md"
      }, [t._v("Notifications")])], 1), a("ul", {
        staticClass: "dropdown-menu dropdown-menu-right"
      }, [a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("Mike John responded to your email")])]), a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("You have 5 new tasks")])]), a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("You're now friend with Andrew")])]), a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("Another Notification")])]), a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("Another One")])])])], 1)], 1)])]), a("md-list-item", {
        on: {
          click: t.goToUsers
        }
      }, [a("i", {
        staticClass: "material-icons"
      }, [t._v("person")]), a("p", {
        staticClass: "hidden-lg hidden-md"
      }, [t._v("Profile")])])], 1)], 1)], 1)])]);
    }),
        ft = [],
        vt = {
      data: function data() {
        return {
          selectedEmployee: "",
          employees: ["Jim Halpert", "Dwight Schrute", "Michael Scott", "Pam Beesly", "Angela Martin", "Kelly Kapoor", "Ryan Howard", "Kevin Malone"]
        };
      },
      methods: {
        toggleSidebar: function toggleSidebar() {
          this.$sidebar.displaySidebar(!this.$sidebar.showSidebar);
        },
        minimizeSidebar: function minimizeSidebar() {
          this.$sidebar && this.$sidebar.toggleMinimize();
        },
        goToNotifications: function goToNotifications() {
          this.$router.push({
            name: "Notifications"
          });
        },
        goToUsers: function goToUsers() {
          this.$router.push({
            name: "User Profile"
          });
        }
      }
    },
        bt = vt,
        gt = Object(d["a"])(bt, ht, ft, !1, null, null, null),
        _t = gt.exports,
        Ct = function Ct() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("footer", {
        staticClass: "footer"
      }, [a("div", {
        staticClass: "container"
      }, [a("nav", [a("ul", [a("li", [t.$route.meta.rtlActive ? a("a", {
        attrs: {
          href: "https://www.creative-tim.com",
          target: "_blank"
        }
      }, [t._v(" منزل ")]) : a("a", {
        attrs: {
          href: "https://www.creative-tim.com",
          target: "_blank"
        }
      }, [t._v(" Creative Tim ")])]), t._m(0), a("li", [t.$route.meta.rtlActive ? a("a", {
        attrs: {
          href: "http://blog.creative-tim.com",
          target: "_blank"
        }
      }, [t._v(" شركة ")]) : a("a", {
        attrs: {
          href: "https://creative-tim.com/presentation"
        }
      }, [t._v(" About Us ")])]), a("li", [t.$route.meta.rtlActive ? a("a", {
        attrs: {
          href: "http://blog.creative-tim.com",
          target: "_blank"
        }
      }, [t._v(" محفظة ")]) : a("a", {
        attrs: {
          href: "http://blog.creative-tim.com",
          target: "_blank"
        }
      }, [t._v(" Blog ")])]), a("li", [t.$route.meta.rtlActive ? a("a", {
        attrs: {
          href: "https://www.creative-tim.com/license",
          target: "_blank"
        }
      }, [t._v(" بلوق ")]) : a("a", {
        attrs: {
          href: "https://www.creative-tim.com/license",
          target: "_blank"
        }
      }, [t._v(" Licenses ")])])])]), a("div", {
        staticClass: "copyright text-center"
      }, [t._v(" © " + t._s(new Date().getFullYear()) + ", made with "), a("i", {
        staticClass: "fa fa-heart heart"
      }), t._v(" by "), a("a", {
        attrs: {
          href: "https://www.creative-tim.com/?ref=mdp-vuejs-api",
          target: "_blank"
        }
      }, [t._v("Creative Tim")]), t._v(" and "), a("a", {
        attrs: {
          href: "https://www.updivision.com/?ref=mdp-vuejs-api",
          target: "_blank"
        }
      }, [t._v("UPDIVISION")]), t._v(" for a better web. ")])])]);
    },
        yt = [function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("li", [a("a", {
        attrs: {
          href: "https://www.updivision.com/",
          target: "_blank"
        }
      }, [t._v(" UPDIVISION ")])]);
    }],
        wt = {},
        kt = wt,
        xt = Object(d["a"])(kt, Ct, yt, !1, null, null, null),
        St = xt.exports,
        $t = function $t() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("ul", {
        staticClass: "nav nav-mobile-menu"
      }, [a("li", [a("md-field", [a("label", [t._v("Search")]), a("md-input", {
        attrs: {
          type: "text"
        },
        model: {
          value: t.search,
          callback: function callback(e) {
            t.search = e;
          },
          expression: "search"
        }
      })], 1)], 1), t._m(0), a("li", [a("drop-down", [a("a", {
        staticClass: "dropdown-toggle",
        attrs: {
          slot: "title",
          "data-toggle": "dropdown"
        },
        slot: "title"
      }, [a("i", {
        staticClass: "material-icons"
      }, [t._v("notifications")]), a("span", {
        staticClass: "notification"
      }, [t._v("5")]), a("p", [t._v("Some Actions")])]), a("ul", {
        staticClass: "dropdown-menu dropdown-menu-right"
      }, [a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("Mike John responded to your email")])]), a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("You have 5 new tasks")])]), a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("You're now friend with Andrew")])]), a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("Another Notification")])]), a("li", [a("a", {
        attrs: {
          href: "#"
        }
      }, [t._v("Another One")])])])])], 1), t._m(1)]);
    },
        Tt = [function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("li", [a("a", {
        staticClass: "dropdown-toggle",
        attrs: {
          href: "#",
          "data-toggle": "dropdown"
        }
      }, [a("i", {
        staticClass: "material-icons"
      }, [t._v("dashboard")]), a("p", [t._v("Stats")])])]);
    }, function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("li", [a("a", {
        staticClass: "dropdown-toggle",
        attrs: {
          href: "#",
          "data-toggle": "dropdown"
        }
      }, [a("i", {
        staticClass: "material-icons"
      }, [t._v("person")]), a("p", [t._v("Account")])])]);
    }],
        Pt = {
      data: function data() {
        return {
          search: null,
          selectedEmployee: null,
          employees: ["Jim Halpert", "Dwight Schrute", "Michael Scott", "Pam Beesly", "Angela Martin", "Kelly Kapoor", "Ryan Howard", "Kevin Malone"]
        };
      }
    },
        jt = Pt,
        Ot = Object(d["a"])(jt, $t, Tt, !1, null, null, null),
        It = Ot.exports,
        zt = function zt() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        directives: [{
          name: "click-outside",
          rawName: "v-click-outside",
          value: t.closeDropDown,
          expression: "closeDropDown"
        }],
        staticClass: "fixed-plugin"
      }, [a("div", {
        staticClass: "dropdown show-dropdown",
        "class": {
          show: t.isOpen
        }
      }, [a("a", {
        attrs: {
          "data-toggle": "dropdown"
        }
      }, [a("i", {
        staticClass: "fa fa-cog fa-2x",
        on: {
          click: t.toggleDropDown
        }
      })]), a("ul", {
        staticClass: "dropdown-menu",
        "class": {
          show: t.isOpen
        }
      }, [a("li", {
        staticClass: "header-title"
      }, [t._v("Sidebar Filters")]), a("li", {
        staticClass: "adjustments-line text-center"
      }, t._l(t.sidebarColors, function (e) {
        return a("span", {
          key: e.color,
          staticClass: "badge filter",
          "class": ["badge-" + e.color, {
            active: e.active
          }],
          attrs: {
            "data-color": e.color
          },
          on: {
            click: function click(a) {
              return t.changeSidebarBackground(e);
            }
          }
        });
      }), 0), a("li", {
        staticClass: "header-title"
      }, [t._v("Sidebar Background")]), a("li", {
        staticClass: "adjustments-line text-center"
      }, t._l(t.sidebarBg, function (e) {
        return a("span", {
          key: e.colorBg,
          staticClass: "badge filter",
          "class": ["badge-" + e.colorBg, {
            active: e.active
          }],
          attrs: {
            "data-color": e.colorBg
          },
          on: {
            click: function click(a) {
              return t.changeSidebarBg(e);
            }
          }
        });
      }), 0), a("li", {
        staticClass: "adjustments-line sidebar-mini"
      }, [t._v(" Sidebar Mini "), a("md-switch", {
        attrs: {
          value: !t.sidebarMini
        },
        on: {
          change: function change(e) {
            return t.updateValue("sidebarMini", e);
          }
        }
      })], 1), a("li", {
        staticClass: "adjustments-line sidebar-img"
      }, [t._v(" Sidebar Image "), a("md-switch", {
        attrs: {
          value: !t.sidebarImg
        },
        on: {
          change: function change(e) {
            return t.updateValueImg("sidebarImg", e);
          }
        }
      })], 1), a("li", {
        staticClass: "header-title"
      }, [t._v("Images")]), t._l(t.sidebarImages, function (e) {
        return a("li", {
          key: e.image,
          "class": {
            active: e.active
          },
          on: {
            click: function click(a) {
              return t.changeSidebarImage(e);
            }
          }
        }, [a("a", {
          staticClass: "img-holder switch-trigger"
        }, [a("img", {
          attrs: {
            src: e.image,
            alt: ""
          }
        })])]);
      }), a("li", {
        staticClass: "button-container"
      }, [a("div", {}, [a("md-button", {
        staticClass: "md-success md-block",
        attrs: {
          href: t.downloadUrl,
          target: "_blank"
        }
      }, [t._v("Download Now ")])], 1)]), a("li", {
        staticClass: "button-container"
      }, [a("div", {}, [a("md-button", {
        staticClass: "md-default md-block",
        attrs: {
          href: t.documentationLink,
          target: "_blank"
        }
      }, [t._v("Documentation ")])], 1)]), a("li", {
        staticClass: "button-container"
      }, [a("div", {}, [a("md-button", {
        staticClass: "md-block md-danger",
        attrs: {
          href: t.upgradeUrl,
          target: "_blank"
        }
      }, [t._v("Upgrade to PRO ")])], 1)]), a("li", {
        staticClass: "github-buttons"
      }, [a("gh-btns-star", {
        attrs: {
          slug: "creativetimofficial/vue-material-dashboard-laravel",
          "show-count": ""
        }
      })], 1), a("li", {
        staticClass: "header-title d-flex justify-content-center"
      }, [t._v(" Thank you for sharing! ")]), a("li", {
        staticClass: "button-container"
      }, [a("social-sharing", {
        attrs: {
          url: t.shareUrl,
          title: "Vue Material Dashboard - Premium Admin Template for Vue.js",
          hashtags: "vuejs, dashboard, vuematerial",
          "twitter-user": "creativetim"
        },
        inlineTemplate: {
          render: function render() {
            var t = this,
                e = t.$createElement,
                a = t._self._c || e;
            return a("div", {
              staticClass: "centered-buttons"
            }, [a("network", {
              staticClass: "md-button md-round md-twitter",
              attrs: {
                network: "twitter",
                url: "https://vue-material-dashboard-laravel.creative-tim.com/"
              }
            }, [a("i", {
              staticClass: "fab fa-twitter"
            }), t._v(" · 45 ")]), a("network", {
              staticClass: "md-button md-round md-facebook",
              attrs: {
                network: "facebook",
                url: "https://vue-material-dashboard-laravel.creative-tim.com/"
              }
            }, [a("i", {
              staticClass: "fab fa-facebook-f"
            }), t._v(" · 50 ")])], 1);
          },
          staticRenderFns: []
        }
      })], 1)], 2)])]);
    },
        Et = [],
        At = a("5299"),
        Dt = a.n(At),
        Mt = a("f676");

    a("3a06");
    s["default"].use(Dt.a), s["default"].use(Mt["a"], {
      useCache: !0
    });

    var Lt = {
      props: {
        sidebarMini: Boolean,
        sidebarImg: Boolean
      },
      data: function data() {
        return {
          documentationLink: "https://vue-material-dashboard-laravel.creative-tim.com/documentation/",
          shareUrl: "https://www.creative-tim.com/product/vue-material-dashboard-laravel",
          buyUrl: "",
          downloadUrl: "https://www.creative-tim.com/product/vue-material-dashboard-laravel",
          upgradeUrl: "https://www.creative-tim.com/product/vue-material-dashboard-laravel-pro",
          isOpen: !1,
          backgroundImage: "".concat("http://localhost:8080/", "/img/sidebar-2.jpg"),
          sidebarColors: [{
            color: "purple",
            active: !1
          }, {
            color: "azure",
            active: !1
          }, {
            color: "green",
            active: !0
          }, {
            color: "orange",
            active: !1
          }, {
            color: "rose",
            active: !1
          }, {
            color: "danger",
            active: !1
          }],
          sidebarBg: [{
            colorBg: "black",
            active: !0
          }, {
            colorBg: "white",
            active: !1
          }, {
            colorBg: "red",
            active: !1
          }],
          sidebarImages: [{
            image: "".concat("http://localhost:8080/", "/img/sidebar-1.jpg"),
            active: !1
          }, {
            image: "".concat("http://localhost:8080/", "/img/sidebar-2.jpg"),
            active: !0
          }, {
            image: "".concat("http://localhost:8080/", "/img/sidebar-3.jpg"),
            active: !1
          }, {
            image: "".concat("http://localhost:8080/", "/img/sidebar-4.jpg"),
            active: !1
          }]
        };
      },
      methods: {
        toggleDropDown: function toggleDropDown() {
          this.isOpen = !this.isOpen;
        },
        closeDropDown: function closeDropDown() {
          this.isOpen = !1;
        },
        toggleList: function toggleList(t, e) {
          t.forEach(function (t) {
            t.active = !1;
          }), e.active = !0;
        },
        updateValue: function updateValue(t, e) {
          this.$emit("update:".concat(t), e);
        },
        updateValueImg: function updateValueImg(t, e) {
          this.$emit("update:".concat(t), e), this.sidebarImg ? (document.body.classList.toggle("sidebar-image"), this.$emit("update:image", "")) : (document.body.classList.toggle("sidebar-image"), this.$emit("update:image", this.backgroundImage));
        },
        changeSidebarBackground: function changeSidebarBackground(t) {
          this.$emit("update:color", t.color), this.toggleList(this.sidebarColors, t);
        },
        changeSidebarBg: function changeSidebarBg(t) {
          this.$emit("update:colorBg", t.colorBg), this.toggleList(this.sidebarBg, t);
        },
        changeSidebarImage: function changeSidebarImage(t) {
          this.sidebarImg && this.$emit("update:image", t.image), this.backgroundImage = t.image, this.toggleList(this.sidebarImages, t);
        }
      }
    },
        Bt = Lt,
        Nt = (a("57e7"), Object(d["a"])(Bt, zt, Et, !1, null, null, null)),
        Rt = Nt.exports,
        Vt = function Vt() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "user"
      }, [a("div", {
        staticClass: "photo"
      }, [a("img", {
        attrs: {
          src: t.avatar,
          alt: "avatar"
        }
      })]), a("div", {
        staticClass: "user-info"
      }, [a("a", {
        attrs: {
          "data-toggle": "collapse",
          "aria-expanded": !t.isClosed
        },
        on: {
          click: function click(e) {
            return e.stopPropagation(), t.toggleMenu(e);
          },
          "!click": function click(e) {
            return t.clicked(e);
          }
        }
      }, [t.$route.meta.rtlActive ? a("span", [t._v(" " + t._s(t.rtlTitle) + " "), a("b", {
        staticClass: "caret"
      })]) : a("span", [t._v(" " + t._s(t.title) + " "), a("b", {
        staticClass: "caret"
      })])]), a("collapse-transition", [a("div", {
        directives: [{
          name: "show",
          rawName: "v-show",
          value: !t.isClosed,
          expression: "!isClosed"
        }]
      }, [a("ul", {
        staticClass: "nav"
      }, [t._t("default", [a("li", [t.$route.meta.rtlActive ? a("a", {
        on: {
          click: t.goToProfile
        }
      }, [a("span", {
        staticClass: "sidebar-mini"
      }, [t._v("مع")]), a("span", {
        staticClass: "sidebar-normal"
      }, [t._v("ملف")])]) : a("a", {
        on: {
          click: t.goToProfile
        }
      }, [a("span", {
        staticClass: "sidebar-mini"
      }, [t._v("MP")]), a("span", {
        staticClass: "sidebar-normal"
      }, [t._v("My Profile")])])]), a("li", [t.$route.meta.rtlActive ? a("a", {
        on: {
          click: t.logout
        }
      }, [a("span", {
        staticClass: "sidebar-mini"
      }, [t._v("و")]), a("span", {
        staticClass: "sidebar-normal"
      }, [t._v("إعدادات")])]) : a("a", {
        on: {
          click: t.logout
        }
      }, [a("span", {
        staticClass: "sidebar-mini"
      }, [t._v("L")]), a("span", {
        staticClass: "sidebar-normal"
      }, [t._v("Logout")])])])])], 2)])])], 1)]);
    },
        Ut = [],
        Ht = (a("96cf"), a("1da1")),
        Ft = {
      data: function data() {
        return {
          isClosed: !0,
          title: "Profile",
          rtlTitle: "تانيا أندرو",
          avatar: "http://localhost:8080//img/faces/marc.jpg"
        };
      },
      created: function created() {
        var t = this;
        return Object(Ht["a"])(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function e() {
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function (e) {
            while (1) {
              switch (e.prev = e.next) {
                case 0:
                  return t.$store.watch(function () {
                    return t.$store.getters["profile/me"];
                  }, function (e) {
                    t.title = e.name;
                  }), e.next = 3, t.$store.dispatch("profile/me");

                case 3:
                case "end":
                  return e.stop();
              }
            }
          }, e);
        }))();
      },
      methods: {
        clicked: function clicked(t) {
          t.preventDefault();
        },
        toggleMenu: function toggleMenu() {
          this.isClosed = !this.isClosed;
        },
        goToProfile: function goToProfile() {
          this.$router.push({
            name: "User Profile"
          });
        },
        logout: function logout() {
          this.$store.dispatch("logout");
        }
      }
    },
        Kt = Ft,
        Wt = (a("a020"), Object(d["a"])(Kt, Vt, Ut, !1, null, null, null)),
        Yt = Wt.exports;

    function qt(t) {
      return document.getElementsByClassName(t).length > 0;
    }

    function Gt(t) {
      qt(t) ? (new pt["a"](".".concat(t)), document.getElementsByClassName(t)[0].scrollTop = 0) : setTimeout(function () {
        Gt(t);
      }, 100);
    }

    function Jt() {
      var t = document.body.classList,
          e = navigator.platform.startsWith("Win");
      e ? (Gt("sidebar"), Gt("sidebar-wrapper"), Gt("main-panel"), t.add("perfect-scrollbar-on")) : t.add("perfect-scrollbar-off");
    }

    var Xt = {
      components: {
        TopNavbar: _t,
        ContentFooter: St,
        FixedPlugin: Rt,
        MobileMenu: It,
        UserMenu: Yt
      },
      data: function data() {
        return {
          sidebarBackgroundColor: "black",
          sidebarBackground: "green",
          sidebarBackgroundImage: "http://localhost:8080//img/sidebar-2.jpg",
          sidebarMini: !0,
          sidebarImg: !0,
          image: "http://localhost:8080//img/laravel-vue.svg"
        };
      },
      methods: {
        toggleSidebar: function toggleSidebar() {
          this.$sidebar.showSidebar && this.$sidebar.displaySidebar(!1);
        },
        minimizeSidebar: function minimizeSidebar() {
          this.$sidebar && this.$sidebar.toggleMinimize();
        }
      },
      updated: function updated() {
        Jt();
      },
      mounted: function mounted() {
        Jt();
      },
      watch: {
        sidebarMini: function sidebarMini() {
          this.minimizeSidebar();
        }
      }
    },
        Qt = Xt,
        Zt = (a("e87a"), Object(d["a"])(Qt, mt, ut, !1, null, null, null)),
        te = Zt.exports,
        ee = function ee() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "full-page",
        "class": {
          "nav-open": t.$sidebar.showSidebar
        }
      }, [a("md-toolbar", {
        staticClass: "md-transparent md-toolbar-absolute",
        attrs: {
          "md-elevation": "0"
        }
      }, [a("div", {
        staticClass: "md-toolbar-row md-offset"
      }, [a("div", {
        staticClass: "md-toolbar-section-start"
      }, [a("h3", {
        staticClass: "md-title"
      }, [t._v(t._s(t.$route.name))])]), a("div", {
        staticClass: "md-toolbar-section-end"
      }, [a("md-button", {
        staticClass: "md-just-icon md-simple md-round md-toolbar-toggle",
        "class": {
          toggled: t.$sidebar.showSidebar
        },
        on: {
          click: t.toggleSidebar
        }
      }, [a("span", {
        staticClass: "icon-bar"
      }), a("span", {
        staticClass: "icon-bar"
      }), a("span", {
        staticClass: "icon-bar"
      })]), a("div", {
        staticClass: "md-collapse",
        "class": {
          "off-canvas-sidebar": t.responsive
        }
      }, [a("md-list", [a("md-list-item", {
        attrs: {
          href: "/register"
        },
        on: {
          click: t.linkClick
        }
      }, [a("md-icon", [t._v("person_add")]), t._v(" Register ")], 1), t.$store.getters["isAuthenticated"] ? t._e() : a("md-list-item", {
        attrs: {
          href: "/login"
        },
        on: {
          click: t.linkClick
        }
      }, [a("md-icon", [t._v("fingerprint")]), t._v(" Login ")], 1)], 1)], 1)], 1)])]), a("div", {
        staticClass: "wrapper wrapper-full-page",
        on: {
          click: t.toggleSidebarPage
        }
      }, [a("notifications"), a("div", {
        staticClass: "page-header header-filter",
        "class": t.setPageClass,
        style: t.setBgImage,
        attrs: {
          "filter-color": "black"
        }
      }, [a("div", {
        staticClass: "container md-offset"
      }, [a("zoom-center-transition", {
        attrs: {
          duration: t.pageTransitionDuration,
          mode: "out-in"
        }
      }, [a("router-view")], 1)], 1), a("footer", {
        staticClass: "footer"
      }, [a("div", {
        staticClass: "container md-offset"
      }, [t._m(0), a("div", {
        staticClass: "copyright text-center"
      }, [t._v(" © " + t._s(new Date().getFullYear()) + ", made with "), a("i", {
        staticClass: "fa fa-heart heart"
      }), t._v(" by "), a("a", {
        attrs: {
          href: "https://www.creative-tim.com/?ref=mdp-vuejs-api",
          target: "_blank"
        }
      }, [t._v("Creative Tim")]), t._v(" and "), a("a", {
        attrs: {
          href: "https://www.updivision.com/?ref=mdp-vuejs-api",
          target: "_blank"
        }
      }, [t._v("UPDIVISION")]), t._v(" for a better web. ")])])])])], 1)], 1);
    },
        ae = [function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("nav", [a("ul", [a("li", [a("a", {
        attrs: {
          href: "https://www.creative-tim.com",
          target: "_blank"
        }
      }, [t._v(" Creative Tim ")])]), a("li", [a("a", {
        attrs: {
          href: "https://www.updivision.com/",
          target: "_blank"
        }
      }, [t._v(" UPDIVISION ")])]), a("li", [a("a", {
        attrs: {
          href: "https://creative-tim.com/presentation",
          target: "_blank"
        }
      }, [t._v(" About Us ")])]), a("li", [a("a", {
        attrs: {
          href: "http://blog.creative-tim.com",
          target: "_blank"
        }
      }, [t._v(" Blog ")])]), a("li", [a("a", {
        attrs: {
          href: "https://www.creative-tim.com/license",
          target: "_blank"
        }
      }, [t._v(" Licenses ")])])])]);
    }],
        se = {
      props: {
        backgroundColor: {
          type: String,
          "default": "black"
        }
      },
      inject: {
        autoClose: {
          "default": !0
        }
      },
      data: function data() {
        return {
          responsive: !1,
          showMenu: !1,
          menuTransitionDuration: 250,
          pageTransitionDuration: 300,
          year: new Date().getFullYear()
        };
      },
      computed: {
        setBgImage: function setBgImage() {
          var t = {
            Login: "http://localhost:8080//img/login.jpg",
            Register: "http://localhost:8080//img/register.jpg"
          };
          return {
            backgroundImage: "url(".concat(t[this.$route.name], ")")
          };
        },
        setPageClass: function setPageClass() {
          return "".concat(this.$route.name, "-page").toLowerCase();
        }
      },
      methods: {
        toggleSidebarPage: function toggleSidebarPage() {
          this.$sidebar.showSidebar && this.$sidebar.displaySidebar(!1);
        },
        linkClick: function linkClick() {
          this.autoClose && this.$sidebar && !0 === this.$sidebar.showSidebar && this.$sidebar.displaySidebar(!1);
        },
        toggleSidebar: function toggleSidebar() {
          this.$sidebar.displaySidebar(!this.$sidebar.showSidebar);
        },
        toggleNavbar: function toggleNavbar() {
          document.body.classList.toggle("nav-open"), this.showMenu = !this.showMenu;
        },
        closeMenu: function closeMenu() {
          document.body.classList.remove("nav-open"), this.showMenu = !1;
        },
        onResponsiveInverted: function onResponsiveInverted() {
          window.innerWidth < 991 ? this.responsive = !0 : this.responsive = !1;
        }
      },
      mounted: function mounted() {
        this.onResponsiveInverted(), window.addEventListener("resize", this.onResponsiveInverted);
      },
      beforeDestroy: function beforeDestroy() {
        this.closeMenu(), window.removeEventListener("resize", this.onResponsiveInverted);
      },
      beforeRouteUpdate: function beforeRouteUpdate(t, e, a) {
        this.showMenu ? (this.closeMenu(), setTimeout(function () {
          a();
        }, this.menuTransitionDuration)) : a();
      }
    },
        ie = se,
        ne = (a("951c"), Object(d["a"])(ie, ee, ae, !1, null, "69cdef9e", null)),
        re = ne.exports,
        oe = function oe() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item md-medium-size-50 md-xsmall-size-100 md-size-25"
      }, [a("stats-card", {
        attrs: {
          "header-color": "blue"
        }
      }, [a("template", {
        slot: "header"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("i", {
        staticClass: "fab fa-twitter"
      })]), a("p", {
        staticClass: "category"
      }, [t._v("Folowers")]), a("h3", {
        staticClass: "title"
      }, [t._v(" +245 ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("update")]), t._v(" Just Updated ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-50 md-xsmall-size-100 md-size-25"
      }, [a("stats-card", {
        attrs: {
          "header-color": "rose"
        }
      }, [a("template", {
        slot: "header"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("equalizer")])], 1), a("p", {
        staticClass: "category"
      }, [t._v("Website Visits")]), a("h3", {
        staticClass: "title"
      }, [t._v(" 75.521 ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("local_offer")]), t._v(" Tracked from Google Analytics ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-50 md-xsmall-size-100 md-size-25"
      }, [a("stats-card", {
        attrs: {
          "header-color": "green"
        }
      }, [a("template", {
        slot: "header"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("store")])], 1), a("p", {
        staticClass: "category"
      }, [t._v("Revenue")]), a("h3", {
        staticClass: "title"
      }, [t._v(" $ 34.245 ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("date_range")]), t._v(" Last 24 Hours ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-50 md-xsmall-size-100 md-size-25"
      }, [a("stats-card", {
        attrs: {
          "header-color": "warning"
        }
      }, [a("template", {
        slot: "header"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("weekend")])], 1), a("p", {
        staticClass: "category"
      }, [t._v("Bookings")]), a("h3", {
        staticClass: "title"
      }, [t._v(" 184 ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", {
        staticClass: "text-danger"
      }, [t._v("warning")]), a("a", {
        attrs: {
          href: "#pablo"
        }
      }, [t._v("Get More Space...")])], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-33"
      }, [a("chart-card", {
        attrs: {
          "chart-data": t.emailsSubscriptionChart.data,
          "chart-options": t.emailsSubscriptionChart.options,
          "chart-responsive-options": t.emailsSubscriptionChart.responsiveOptions,
          "chart-type": "Bar",
          "chart-inside-header": "",
          "background-color": "rose"
        }
      }, [a("md-icon", {
        attrs: {
          slot: "fixed-button"
        },
        slot: "fixed-button"
      }, [t._v("build")]), a("md-button", {
        staticClass: "md-simple md-info md-just-icon",
        attrs: {
          slot: "first-button"
        },
        slot: "first-button"
      }, [a("md-icon", [t._v("refresh")]), a("md-tooltip", {
        attrs: {
          "md-direction": "bottom"
        }
      }, [t._v("Refresh")])], 1), a("md-button", {
        staticClass: "md-simple md-just-icon",
        attrs: {
          slot: "second-button"
        },
        slot: "second-button"
      }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
        attrs: {
          "md-direction": "bottom"
        }
      }, [t._v("Change Date")])], 1), a("template", {
        slot: "content"
      }, [a("h4", {
        staticClass: "title"
      }, [t._v("Website Views")]), a("p", {
        staticClass: "category"
      }, [t._v(" Last Campaign Performance ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("access_time")]), t._v(" updated 10 days ago ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-33"
      }, [a("chart-card", {
        attrs: {
          "chart-data": t.dailySalesChart.data,
          "chart-options": t.dailySalesChart.options,
          "chart-type": "Line",
          "chart-inside-header": "",
          "background-color": "green"
        }
      }, [a("md-button", {
        staticClass: "md-simple md-info md-just-icon",
        attrs: {
          slot: "first-button"
        },
        slot: "first-button"
      }, [a("md-icon", [t._v("refresh")]), a("md-tooltip", {
        attrs: {
          "md-direction": "bottom"
        }
      }, [t._v("Refresh")])], 1), a("md-button", {
        staticClass: "md-simple md-just-icon",
        attrs: {
          slot: "second-button"
        },
        slot: "second-button"
      }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
        attrs: {
          "md-direction": "bottom"
        }
      }, [t._v("Change Date")])], 1), a("template", {
        slot: "content"
      }, [a("h4", {
        staticClass: "title"
      }, [t._v("Daily Sales")]), a("p", {
        staticClass: "category"
      }, [a("span", {
        staticClass: "text-success"
      }, [a("i", {
        staticClass: "fas fa-long-arrow-alt-up"
      }), t._v(" 55% ")]), t._v(" increase in today sales. ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("access_time")]), t._v(" updated 4 minutes ago ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-33"
      }, [a("chart-card", {
        attrs: {
          "chart-data": t.dataCompletedTasksChart.data,
          "chart-options": t.dataCompletedTasksChart.options,
          "chart-type": "Line",
          "chart-inside-header": "",
          "background-color": "blue"
        }
      }, [a("md-button", {
        staticClass: "md-simple md-info md-just-icon",
        attrs: {
          slot: "first-button"
        },
        slot: "first-button"
      }, [a("md-icon", [t._v("refresh")]), a("md-tooltip", {
        attrs: {
          "md-direction": "bottom"
        }
      }, [t._v("Refresh")])], 1), a("md-button", {
        staticClass: "md-simple md-just-icon",
        attrs: {
          slot: "second-button"
        },
        slot: "second-button"
      }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
        attrs: {
          "md-direction": "bottom"
        }
      }, [t._v("Change Date")])], 1), a("template", {
        slot: "content"
      }, [a("h4", {
        staticClass: "title"
      }, [t._v("Completed Tasks")]), a("p", {
        staticClass: "category"
      }, [t._v(" Last Campaign Performance ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("access_time")]), t._v(" campaign sent 26 minutes ago ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-50"
      }, [a("md-card", [a("md-card-header", {
        staticClass: "md-card-header-text md-card-header-warning"
      }, [a("div", {
        staticClass: "card-text"
      }, [a("h4", {
        staticClass: "title"
      }, [t._v("Employees Stats")]), a("p", {
        staticClass: "category"
      }, [t._v(" New employees on 15th September, 2016 ")])])]), a("md-card-content", [a("md-table", {
        attrs: {
          "table-header-color": "orange"
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {}, [a("md-table-cell", {
              attrs: {
                "md-label": "Id"
              }
            }, [t._v(t._s(s.id))]), a("md-table-cell", {
              attrs: {
                "md-label": "Name"
              }
            }, [t._v(t._s(s.name))]), a("md-table-cell", {
              attrs: {
                "md-label": "Salary"
              }
            }, [t._v(t._s(s.salary))]), a("md-table-cell", {
              attrs: {
                "md-label": "Country"
              }
            }, [t._v(t._s(s.country))])], 1);
          }
        }]),
        model: {
          value: t.users,
          callback: function callback(e) {
            t.users = e;
          },
          expression: "users"
        }
      })], 1)], 1)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-50"
      }, [a("nav-tabs-card", [a("template", {
        slot: "content"
      }, [a("span", {
        staticClass: "md-nav-tabs-title"
      }, [t._v("Tasks")]), a("md-tabs", {
        staticClass: "md-rose",
        attrs: {
          "md-sync-route": "",
          "md-alignment": "left"
        }
      }, [a("md-tab", {
        attrs: {
          id: "tab-home",
          "md-label": "Bugs",
          "md-icon": "bug_report"
        }
      }, [a("md-table", {
        on: {
          "md-selected": t.onSelect
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {
              attrs: {
                "md-selectable": "multiple",
                "md-auto-select": ""
              }
            }, [a("md-table-cell", [t._v(t._s(s.tab))]), a("md-table-cell", [a("md-button", {
              staticClass: "md-just-icon md-simple md-primary"
            }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Edit")])], 1), a("md-button", {
              staticClass: "md-just-icon md-simple md-danger"
            }, [a("md-icon", [t._v("close")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Close")])], 1)], 1)], 1);
          }
        }]),
        model: {
          value: t.firstTabs,
          callback: function callback(e) {
            t.firstTabs = e;
          },
          expression: "firstTabs"
        }
      })], 1), a("md-tab", {
        attrs: {
          id: "tab-pages",
          "md-label": "Website",
          "md-icon": "code"
        }
      }, [a("md-table", {
        on: {
          "md-selected": t.onSelect
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {
              attrs: {
                "md-selectable": "multiple",
                "md-auto-select": ""
              }
            }, [a("md-table-cell", [t._v(t._s(s.tab))]), a("md-table-cell", [a("md-button", {
              staticClass: "md-just-icon md-simple md-primary"
            }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Edit")])], 1), a("md-button", {
              staticClass: "md-just-icon md-simple md-danger"
            }, [a("md-icon", [t._v("close")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Close")])], 1)], 1)], 1);
          }
        }]),
        model: {
          value: t.secondTabs,
          callback: function callback(e) {
            t.secondTabs = e;
          },
          expression: "secondTabs"
        }
      })], 1), a("md-tab", {
        attrs: {
          id: "tab-posts",
          "md-label": "Server",
          "md-icon": "cloud"
        }
      }, [a("md-table", {
        on: {
          "md-selected": t.onSelect
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {
              attrs: {
                "md-selectable": "multiple",
                "md-auto-select": ""
              }
            }, [a("md-table-cell", [t._v(t._s(s.tab))]), a("md-table-cell", [a("md-button", {
              staticClass: "md-just-icon md-simple md-primary"
            }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Edit")])], 1), a("md-button", {
              staticClass: "md-just-icon md-simple md-danger"
            }, [a("md-icon", [t._v("close")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Close")])], 1)], 1)], 1);
          }
        }]),
        model: {
          value: t.thirdTabs,
          callback: function callback(e) {
            t.thirdTabs = e;
          },
          expression: "thirdTabs"
        }
      })], 1)], 1)], 1)], 2)], 1)]);
    },
        le = [],
        ce = function ce() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a(t.tag, {
        tag: "component",
        staticClass: "badge",
        "class": "badge-" + t.type
      }, [t._t("default")], 2);
    },
        de = [],
        me = {
      name: "badge",
      props: {
        tag: {
          type: String,
          "default": "span"
        },
        type: {
          type: String,
          "default": "default",
          validator: function validator(t) {
            var e = ["primary", "info", "success", "warning", "danger", "rose"];
            return -1 !== e.indexOf(t);
          }
        }
      }
    },
        ue = me,
        pe = Object(d["a"])(ue, ce, de, !1, null, null, null),
        he = (pe.exports, function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("md-card", {
        staticClass: "md-card-signup"
      }, [t._t("title"), a("md-card-content", [a("div", {
        staticClass: "md-layout"
      }, [t._t("content-left"), t._t("content-right")], 2)]), a("md-card-actions", [t._t("footer")], 2)], 2);
    }),
        fe = [],
        ve = {
      name: "signup-card"
    },
        be = ve,
        ge = Object(d["a"])(be, he, fe, !1, null, null, null),
        _e = ge.exports,
        Ce = function Ce() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("md-card", {
        staticClass: "md-card-login",
        "class": {
          "md-card-hidden": t.cardHidden
        }
      }, [a("md-card-header", {
        "class": t.getClass(t.headerColor)
      }, [t._t("title"), a("div", {
        staticClass: "social-line"
      }, [t._t("buttons")], 2)], 2), a("md-card-content", [t._t("description"), t._t("inputs")], 2), a("md-card-actions", [t._t("footer")], 2)], 1);
    },
        ye = [],
        we = {
      name: "login-card",
      props: {
        headerColor: {
          type: String,
          "default": ""
        }
      },
      data: function data() {
        return {
          cardHidden: !0
        };
      },
      beforeMount: function beforeMount() {
        setTimeout(this.showCard, 400);
      },
      methods: {
        showCard: function showCard() {
          this.cardHidden = !1;
        },
        getClass: function getClass(t) {
          return "md-card-header-" + t;
        }
      }
    },
        ke = we,
        xe = Object(d["a"])(ke, Ce, ye, !1, null, null, null),
        Se = xe.exports,
        $e = function $e() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("md-card", {
        staticClass: "md-card-stats"
      }, [a("md-card-header", {
        staticClass: "md-card-header-icon",
        "class": t.getClass(t.headerColor)
      }, [t._t("header")], 2), a("md-card-actions", {
        attrs: {
          "md-alignment": "left"
        }
      }, [t._t("footer")], 2)], 1);
    },
        Te = [],
        Pe = {
      name: "stats-card",
      props: {
        headerColor: {
          type: String,
          "default": ""
        }
      },
      methods: {
        getClass: function getClass(t) {
          return "md-card-header-" + t;
        }
      }
    },
        je = Pe,
        Oe = Object(d["a"])(je, $e, Te, !1, null, null, null),
        Ie = Oe.exports,
        ze = function ze() {
      var t,
          e = this,
          a = e.$createElement,
          s = e._self._c || a;
      return s("md-card", {
        staticClass: "md-card-chart",
        attrs: {
          "data-count": e.hoverCount
        }
      }, [s("md-card-header", {
        "class": [(t = {}, t[e.getClass(e.backgroundColor)] = !0, t), {
          "md-card-header-text": e.HeaderText
        }, {
          "md-card-header-icon": e.HeaderIcon
        }]
      }, [e.chartInsideHeader ? s("div", {
        staticClass: "ct-chart",
        attrs: {
          id: e.chartId
        }
      }) : e._e(), e._t("chartInsideHeader")], 2), s("md-card-content", [e.chartInsideContent ? s("div", {
        staticClass: "ct-chart",
        attrs: {
          id: e.chartId
        }
      }) : e._e(), e._t("content")], 2), e.noFooter ? e._e() : s("md-card-actions", {
        attrs: {
          "md-alignment": "left"
        }
      }, [e._t("footer")], 2)], 1);
    },
        Ee = [],
        Ae = (a("d3b7"), a("25f0"), {
      name: "chart-card",
      props: {
        HeaderText: Boolean,
        HeaderIcon: Boolean,
        noFooter: Boolean,
        chartInsideContent: Boolean,
        chartInsideHeader: Boolean,
        chartType: {
          type: String,
          "default": "Line"
        },
        chartOptions: {
          type: Object,
          "default": function _default() {
            return {};
          }
        },
        chartResponsiveOptions: {
          type: Array,
          "default": function _default() {
            return [];
          }
        },
        chartData: {
          type: Object,
          "default": function _default() {
            return {
              labels: [],
              series: []
            };
          }
        },
        backgroundColor: {
          type: String,
          "default": ""
        }
      },
      data: function data() {
        return {
          hoverCount: 0,
          imgHovered: !1,
          fixedHeader: !1,
          chartId: "no-id"
        };
      },
      computed: {
        headerDown: function headerDown() {
          return this.hoverCount > 15;
        }
      },
      methods: {
        getClass: function getClass(t) {
          return "md-card-header-" + t;
        },
        initChart: function initChart(t) {
          var e = "#".concat(this.chartId);
          t[this.chartType](e, this.chartData, this.chartOptions, this.chartAnimation);
        },
        updateChartId: function updateChartId() {
          var t = new Date().getTime().toString(),
              e = this.getRandomInt(0, t);
          this.chartId = "div_".concat(e);
        },
        getRandomInt: function getRandomInt(t, e) {
          return Math.floor(Math.random() * (e - t + 1)) + t;
        }
      },
      mounted: function mounted() {
        var t = this;
        this.updateChartId(), Promise.resolve().then(a.t.bind(null, "ba48", 7)).then(function (e) {
          var a = e["default"] || e;
          t.$nextTick(function () {
            t.initChart(a);
          });
        });
      }
    }),
        De = Ae,
        Me = Object(d["a"])(De, ze, Ee, !1, null, null, null),
        Le = Me.exports,
        Be = function Be() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("md-card", {
        staticClass: "md-card-nav-tabs"
      }, [a("md-card-content", [t._t("content")], 2)], 1);
    },
        Ne = [],
        Re = {
      name: "nav-tabs-card"
    },
        Ve = Re,
        Ue = Object(d["a"])(Ve, Be, Ne, !1, null, null, null),
        He = Ue.exports,
        Fe = function Fe() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return t.errors && t.errors.length ? a("span", [a("small", {
        staticClass: "md-error",
        domProps: {
          textContent: t._s(t.errors[0])
        }
      })]) : t._e();
    },
        Ke = [],
        We = {
      props: {
        errors: {
          type: Array,
          "default": function _default() {
            return [];
          }
        }
      }
    },
        Ye = We,
        qe = (a("fc63"), Object(d["a"])(Ye, Fe, Ke, !1, null, null, null)),
        Ge = qe.exports,
        Je = function Je() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("ul", {
        staticClass: "pagination",
        "class": t.paginationClass
      }, [a("li", {
        staticClass: "page-item prev-page",
        "class": {
          disabled: 1 === t.value,
          "no-arrows": t.noArrows
        }
      }, [a("a", {
        staticClass: "page-link",
        attrs: {
          "aria-label": "Previous"
        },
        on: {
          click: t.prevPage
        }
      }, [a("i", {
        staticClass: "fas fa-angle-double-left"
      })])]), t._l(t.range(t.minPage, t.maxPage), function (e) {
        return a("li", {
          key: e,
          staticClass: "page-item",
          "class": {
            active: t.value === e
          }
        }, [a("a", {
          staticClass: "page-link",
          on: {
            click: function click(a) {
              return t.changePage(e);
            }
          }
        }, [t._v(t._s(e))])]);
      }), a("li", {
        staticClass: "page-item page-pre next-page",
        "class": {
          disabled: t.value === t.totalPages,
          "no-arrows": t.noArrows
        }
      }, [a("a", {
        staticClass: "page-link",
        attrs: {
          "aria-label": "Next"
        },
        on: {
          click: t.nextPage
        }
      }, [a("i", {
        staticClass: "fas fa-angle-double-right"
      })])])], 2);
    },
        Xe = [],
        Qe = (a("caad"), {
      name: "pagination",
      props: {
        type: {
          type: String,
          "default": "primary",
          validator: function validator(t) {
            return ["default", "primary", "danger", "success", "warning", "info", "rose"].includes(t);
          }
        },
        noArrows: Boolean,
        pageCount: {
          type: Number,
          "default": 0
        },
        perPage: {
          type: Number,
          "default": 10
        },
        total: {
          type: Number,
          "default": 0
        },
        value: {
          type: Number,
          "default": 1
        }
      },
      computed: {
        paginationClass: function paginationClass() {
          return "pagination-".concat(this.type);
        },
        totalPages: function totalPages() {
          return this.pageCount > 0 ? this.pageCount : this.total > 0 ? Math.ceil(this.total / this.perPage) : 1;
        },
        pagesToDisplay: function pagesToDisplay() {
          return this.totalPages > 0 && this.totalPages < this.defaultPagesToDisplay ? this.totalPages : this.defaultPagesToDisplay;
        },
        minPage: function minPage() {
          if (this.value >= this.pagesToDisplay) {
            var t = Math.floor(this.pagesToDisplay / 2),
                e = t + this.value;
            return e > this.totalPages ? this.totalPages - this.pagesToDisplay + 1 : this.value - t;
          }

          return 1;
        },
        maxPage: function maxPage() {
          if (this.value >= this.pagesToDisplay) {
            var t = Math.floor(this.pagesToDisplay / 2),
                e = t + this.value;
            return e < this.totalPages ? e : this.totalPages;
          }

          return this.pagesToDisplay;
        }
      },
      data: function data() {
        return {
          defaultPagesToDisplay: 5
        };
      },
      methods: {
        range: function range(t, e) {
          for (var a = [], s = t; s <= e; s++) {
            a.push(s);
          }

          return a;
        },
        changePage: function changePage(t) {
          this.$emit("input", t);
        },
        nextPage: function nextPage() {
          this.value < this.totalPages && this.$emit("input", this.value + 1);
        },
        prevPage: function prevPage() {
          this.value > 1 && this.$emit("input", this.value - 1);
        }
      },
      watch: {
        perPage: function perPage() {
          this.$emit("input", 1);
        },
        total: function total() {
          this.$emit("input", 1);
        }
      }
    }),
        Ze = Qe,
        ta = Object(d["a"])(Ze, Je, Xe, !1, null, null, null),
        ea = ta.exports,
        aa = {
      components: {
        StatsCard: Ie,
        ChartCard: Le,
        NavTabsCard: He
      },
      data: function data() {
        return {
          product1: "http://localhost:8080//img/card-2.jpg",
          product2: "http://localhost:8080//img/card-3.jpg",
          product3: "http://localhost:8080//img/card-1.jpg",
          seq2: 0,
          selected: [],
          firstTabs: [{
            tab: 'Sign contract for "What are conference organizers afraid of?"'
          }, {
            tab: "Lines From Great Russian Literature? Or E-mails From My Boss?"
          }, {
            tab: "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit"
          }, {
            tab: "Create 4 Invisible User Experiences you Never Knew About"
          }],
          secondTabs: [{
            tab: "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit"
          }, {
            tab: 'Sign contract for "What are conference organizers afraid of?"'
          }],
          thirdTabs: [{
            tab: "Lines From Great Russian Literature? Or E-mails From My Boss?"
          }, {
            tab: "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit"
          }, {
            tab: 'Sign contract for "What are conference organizers afraid of?"'
          }],
          users: [{
            id: 1,
            name: "Noelia O'Kon",
            salary: "13098.00",
            country: "Niger"
          }, {
            id: 2,
            name: "Mr. Enid Von PhD",
            salary: "35978.00",
            country: "Curaçao"
          }, {
            id: 3,
            name: "Colton Koch",
            salary: "26278.00",
            country: "Netherlands"
          }, {
            id: 4,
            name: "Gregory Vandervort",
            salary: "25537.00",
            country: "South Korea"
          }],
          dailySalesChart: {
            data: {
              labels: ["M", "T", "W", "T", "F", "S", "S"],
              series: [[12, 17, 7, 17, 23, 18, 38]]
            },
            options: {
              lineSmooth: this.$Chartist.Interpolation.cardinal({
                tension: 0
              }),
              low: 0,
              high: 50,
              chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
              }
            }
          },
          dataCompletedTasksChart: {
            data: {
              labels: ["12am", "3pm", "6pm", "9pm", "12pm", "3am", "6am", "9am"],
              series: [[230, 750, 450, 300, 280, 240, 200, 190]]
            },
            options: {
              lineSmooth: this.$Chartist.Interpolation.cardinal({
                tension: 0
              }),
              low: 0,
              high: 1e3,
              chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
              }
            }
          },
          emailsSubscriptionChart: {
            data: {
              labels: ["Ja", "Fe", "Ma", "Ap", "Mai", "Ju", "Jul", "Au", "Se", "Oc", "No", "De"],
              series: [[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]]
            },
            options: {
              axisX: {
                showGrid: !1
              },
              low: 0,
              high: 1e3,
              chartPadding: {
                top: 0,
                right: 5,
                bottom: 0,
                left: 0
              }
            },
            responsiveOptions: [["screen and (max-width: 640px)", {
              seriesBarDistance: 5,
              axisX: {
                labelInterpolationFnc: function labelInterpolationFnc(t) {
                  return t[0];
                }
              }
            }]]
          }
        };
      },
      methods: {
        onSelect: function onSelect(t) {
          this.selected = t;
        }
      }
    },
        sa = aa,
        ia = Object(d["a"])(sa, oe, le, !1, null, null, null),
        na = ia.exports,
        ra = function ra() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return t.user ? a("div", {
        staticClass: "md-layout md-gutter"
      }, [a("div", {
        staticClass: "md-layout-item md-size-66 md-small-size-100"
      }, [a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("user-edit-card", {
        attrs: {
          user: t.user
        }
      })], 1), a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("user-password-card", {
        attrs: {
          user: t.user
        }
      })], 1)]), a("div", {
        staticClass: "md-layout-item md-size-33 md-small-size-100"
      }, [a("user-profile-card", {
        attrs: {
          user: t.user
        }
      })], 1)]) : t._e();
    },
        oa = [],
        la = function la() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("form", {
        on: {
          submit: function submit(e) {
            return e.preventDefault(), t.updateProfile(e);
          }
        }
      }, [a("md-card", [a("md-card-header", {
        staticClass: "md-card-header-icon"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("perm_identity")])], 1), a("h4", {
        staticClass: "title"
      }, [t._v(" Edit Profile ")])]), a("md-card-content", [a("div", {
        staticClass: "md-layout"
      }, [a("label", {
        staticClass: "md-layout-item md-size-15 md-form-label"
      }, [t._v(" Name ")]), a("div", {
        staticClass: "md-layout-item"
      }, [a("md-field", {
        staticClass: "md-invalid"
      }, [a("md-input", {
        model: {
          value: t.user.name,
          callback: function callback(e) {
            t.$set(t.user, "name", e);
          },
          expression: "user.name"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.name
        }
      })], 1)], 1)]), a("div", {
        staticClass: "md-layout"
      }, [a("label", {
        staticClass: "md-layout-item md-size-15 md-form-label"
      }, [t._v(" Email ")]), a("div", {
        staticClass: "md-layout-item"
      }, [a("md-field", {
        staticClass: "md-invalid"
      }, [a("md-input", {
        model: {
          value: t.user.email,
          callback: function callback(e) {
            t.$set(t.user, "email", e);
          },
          expression: "user.email"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.email
        }
      })], 1)], 1)])]), a("md-card-actions", [a("md-button", {
        attrs: {
          type: "submit"
        }
      }, [t._v(" Update Profile ")])], 1)], 1)], 1);
    },
        ca = [],
        da = (a("99af"), a("13d5"), a("ade3")),
        ma = (a("2ef0"), {
      data: function data() {
        return {
          isLoading: !1,
          apiValidationErrors: {}
        };
      },
      methods: {
        setApiValidation: function setApiValidation(t) {
          this.apiValidationErrors = t.reduce(function (t, e) {
            if ("undefined" === typeof e.source) return !1;
            var a = e.source.pointer.split("/")[3],
                s = (t[a] || []).concat(e.detail);
            return Object(ct["a"])(Object(ct["a"])({}, t), {}, Object(da["a"])({}, a, s));
          }, {});
        }
      }
    }),
        ua = {
      name: "edit-profile-card",
      props: {
        user: Object
      },
      components: {
        ValidationError: Ge
      },
      mixins: [ma],
      data: function data() {
        return {
          default_img: "http://localhost:8080//img/placeholder.jpg"
        };
      },
      methods: {
        updateProfile: function updateProfile() {
          var t = this;
          return Object(Ht["a"])(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function e() {
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function (e) {
              while (1) {
                switch (e.prev = e.next) {
                  case 0:
                    if (!["1", "2", "3"].includes(t.user.id)) {
                      e.next = 4;
                      break;
                    }

                    return e.next = 3, t.$store.dispatch("alerts/error", "You are not allowed not change data of default users.");

                  case 3:
                    return e.abrupt("return");

                  case 4:
                    return e.prev = 4, e.next = 7, t.$store.dispatch("profile/update", t.user);

                  case 7:
                    return e.next = 9, t.$store.dispatch("alerts/success", "Profile updated successfully.");

                  case 9:
                    return e.next = 11, t.$store.getters["profile/me"];

                  case 11:
                    e.next = 18;
                    break;

                  case 13:
                    return e.prev = 13, e.t0 = e["catch"](4), e.next = 17, t.$store.dispatch("alerts/error", "Oops, something went wrong!");

                  case 17:
                    t.setApiValidation(e.t0.response.data.errors);

                  case 18:
                  case "end":
                    return e.stop();
                }
              }
            }, e, null, [[4, 13]]);
          }))();
        }
      }
    },
        pa = ua,
        ha = Object(d["a"])(pa, la, ca, !1, null, null, null),
        fa = ha.exports,
        va = function va() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("form", {
        ref: "password_form",
        on: {
          submit: function submit(e) {
            return e.preventDefault(), t.changePassword(e);
          }
        }
      }, [a("md-card", [a("md-card-header", {
        staticClass: "md-card-header-icon"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("perm_identity")])], 1), a("h4", {
        staticClass: "title"
      }, [t._v(" Change Password ")])]), a("md-card-content", [a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("md-field", {
        staticClass: "md-invalid"
      }, [a("label", [t._v("Current Password")]), a("md-input", {
        attrs: {
          type: "password"
        },
        model: {
          value: t.password,
          callback: function callback(e) {
            t.password = e;
          },
          expression: "password"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.password
        }
      })], 1), a("md-field", {
        staticClass: "md-invalid"
      }, [a("label", [t._v("New Password")]), a("md-input", {
        attrs: {
          type: "password"
        },
        model: {
          value: t.new_password,
          callback: function callback(e) {
            t.new_password = e;
          },
          expression: "new_password"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.password_confirmation
        }
      })], 1), a("md-field", {
        staticClass: "md-invalid"
      }, [a("label", [t._v("Confirm New Password")]), a("md-input", {
        attrs: {
          type: "password"
        },
        model: {
          value: t.confirm_password,
          callback: function callback(e) {
            t.confirm_password = e;
          },
          expression: "confirm_password"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.password_confirmation
        }
      })], 1)], 1)])]), a("md-card-actions", [a("md-button", {
        attrs: {
          type: "submit"
        }
      }, [t._v(" Change Password ")])], 1)], 1)], 1);
    },
        ba = [],
        ga = {
      name: "edit-password-card",
      props: {
        user: Object
      },
      components: {
        ValidationError: Ge
      },
      mixins: [ma],
      data: function data() {
        return {
          password: null,
          new_password: null,
          confirm_password: null
        };
      },
      methods: {
        changePassword: function changePassword() {
          var t = this;
          return Object(Ht["a"])(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function e() {
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function (e) {
              while (1) {
                switch (e.prev = e.next) {
                  case 0:
                    if (!["1", "2", "3"].includes(t.user.id)) {
                      e.next = 4;
                      break;
                    }

                    return e.next = 3, t.$store.dispatch("alerts/error", "You are not allowed not change data of default users.");

                  case 3:
                    return e.abrupt("return");

                  case 4:
                    return t.user.password = t.password, t.user.password_new = t.new_password, t.user.password_confirmation = t.confirm_password, e.prev = 7, e.next = 10, t.$store.dispatch("users/update", t.user);

                  case 10:
                    return e.next = 12, t.$store.dispatch("alerts/error", "Password changed successfully.");

                  case 12:
                    return e.next = 14, t.$store.getters["profile/me"];

                  case 14:
                    t.user = e.sent, t.$refs["password_form"].reset(), e.next = 23;
                    break;

                  case 18:
                    return e.prev = 18, e.t0 = e["catch"](7), e.next = 22, t.$store.dispatch("alerts/error", "Oops, something went wrong!");

                  case 22:
                    t.setApiValidation(e.t0.response.data.errors);

                  case 23:
                  case "end":
                    return e.stop();
                }
              }
            }, e, null, [[7, 18]]);
          }))();
        }
      }
    },
        _a = ga,
        Ca = Object(d["a"])(_a, va, ba, !1, null, null, null),
        ya = Ca.exports,
        wa = function wa() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("md-card", {
        staticClass: "md-card-profile"
      }, [a("div", {
        staticClass: "md-card-avatar"
      }, [a("img", {
        staticClass: "img",
        attrs: {
          src: t.cardUserImage
        }
      })]), a("md-card-content", [a("h6", {
        staticClass: "category text-gray"
      }, [t._v("CEO / Co-Founder")]), a("h4", {
        staticClass: "card-title"
      }, [t._v("Alec Thompson")]), a("p", {
        staticClass: "card-description"
      }, [t._v(" Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is... ")]), a("md-button", {
        staticClass: "md-round",
        "class": t.getColorButton(t.buttonColor)
      }, [t._v(" Follow ")])], 1)], 1);
    },
        ka = [],
        xa = {
      name: "user-profile-card",
      props: {
        cardUserImage: {
          type: String,
          "default": "http://localhost:8080//img/faces/marc.jpg"
        },
        buttonColor: {
          type: String,
          "default": ""
        }
      },
      data: function data() {
        return {};
      },
      methods: {
        getColorButton: function getColorButton(t) {
          return "md-" + t;
        }
      }
    },
        Sa = xa,
        $a = Object(d["a"])(Sa, wa, ka, !1, null, null, null),
        Ta = $a.exports,
        Pa = {
      name: "user-profile-example",
      components: {
        "user-profile-card": Ta,
        "user-edit-card": fa,
        "user-password-card": ya
      },
      data: function data() {
        return {
          user: null
        };
      },
      created: function created() {
        this.getProfile();
      },
      methods: {
        getProfile: function getProfile() {
          var t = this;
          return Object(Ht["a"])(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function e() {
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function (e) {
              while (1) {
                switch (e.prev = e.next) {
                  case 0:
                    return e.next = 2, t.$store.dispatch("profile/me");

                  case 2:
                    return e.next = 4, t.$store.getters["profile/me"];

                  case 4:
                    t.user = e.sent;

                  case 5:
                  case "end":
                    return e.stop();
                }
              }
            }, e);
          }))();
        }
      }
    },
        ja = Pa,
        Oa = Object(d["a"])(ja, ra, oa, !1, null, null, null),
        Ia = Oa.exports,
        za = function za() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("md-card", [a("md-card-header", {
        staticClass: "md-card-header-icon md-card-header-green"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("assignment")])], 1), a("h4", {
        staticClass: "title"
      }, [t._v("Users List")])]), a("md-card-content", [a("div", {
        staticClass: "text-right"
      }, [a("md-button", {
        staticClass: "md-primary md-dense",
        on: {
          click: t.onProFeature
        }
      }, [t._v(" Add User ")])], 1), a("md-table", {
        staticClass: "paginated-table table-striped table-hover",
        attrs: {
          value: t.table,
          "md-sort": t.sortation.field,
          "md-sort-order": t.sortation.order,
          "md-sort-fn": t.customSort
        },
        on: {
          "update:mdSort": function updateMdSort(e) {
            return t.$set(t.sortation, "field", e);
          },
          "update:md-sort": function updateMdSort(e) {
            return t.$set(t.sortation, "field", e);
          },
          "update:mdSortOrder": function updateMdSortOrder(e) {
            return t.$set(t.sortation, "order", e);
          },
          "update:md-sort-order": function updateMdSortOrder(e) {
            return t.$set(t.sortation, "order", e);
          }
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {}, [a("md-table-cell", {
              attrs: {
                "md-label": "Name",
                "md-sort-by": "name"
              }
            }, [t._v(t._s(s.name))]), a("md-table-cell", {
              attrs: {
                "md-label": "Email",
                "md-sort-by": "email"
              }
            }, [t._v(t._s(s.email))]), a("md-table-cell", {
              attrs: {
                "md-label": "Created At",
                "md-sort-by": "created_at"
              }
            }, [t._v(t._s(s.created_at))]), a("md-table-cell", {
              attrs: {
                "md-label": "Actions"
              }
            }, [a("md-button", {
              staticClass: "md-icon-button md-raised md-round md-info",
              staticStyle: {
                margin: ".2rem"
              },
              on: {
                click: t.onProFeature
              }
            }, [a("md-icon", [t._v("edit")])], 1), a("md-button", {
              staticClass: "md-icon-button md-raised md-round md-danger",
              staticStyle: {
                margin: ".2rem"
              },
              on: {
                click: t.onProFeature
              }
            }, [a("md-icon", [t._v("delete")])], 1)], 1)], 1);
          }
        }])
      }, [a("md-table-toolbar", [a("md-field", [a("label", [t._v("Per page")]), a("md-select", {
        attrs: {
          name: "pages"
        },
        model: {
          value: t.pagination.perPage,
          callback: function callback(e) {
            t.$set(t.pagination, "perPage", e);
          },
          expression: "pagination.perPage"
        }
      }, t._l(t.pagination.perPageOptions, function (e) {
        return a("md-option", {
          key: e,
          attrs: {
            label: e,
            value: e
          }
        }, [t._v(" " + t._s(e) + " ")]);
      }), 1)], 1)], 1)], 1), a("div", {
        staticClass: "footer-table md-table"
      }, [a("table", [a("tfoot", [a("tr", t._l(t.footerTable, function (e) {
        return a("th", {
          key: e.name,
          staticClass: "md-table-head"
        }, [a("div", {
          staticClass: "md-table-head-container md-ripple md-disabled"
        }, [a("div", {
          staticClass: "md-table-head-label"
        }, [t._v(" " + t._s(e) + " ")])])]);
      }), 0)])])])], 1), a("md-card-actions", {
        attrs: {
          "md-alignment": "space-between"
        }
      }, [a("div", {}, [a("p", {
        staticClass: "card-category"
      }, [t._v(" Showing " + t._s(t.from + 1) + " to " + t._s(t.to) + " of " + t._s(t.total) + " entries ")])]), a("pagination", {
        staticClass: "pagination-no-border pagination-success",
        attrs: {
          "per-page": t.pagination.perPage,
          total: t.total
        },
        model: {
          value: t.pagination.currentPage,
          callback: function callback(e) {
            t.$set(t.pagination, "currentPage", e);
          },
          expression: "pagination.currentPage"
        }
      })], 1)], 1)], 1)]);
    },
        Ea = [],
        Aa = {
      components: {
        pagination: ea
      },
      data: function data() {
        return {
          table: [],
          footerTable: ["Name", "Email", "Created At", "Actions"],
          query: null,
          sortation: {
            field: "created_at",
            order: "asc"
          },
          pagination: {
            perPage: 5,
            currentPage: 1,
            perPageOptions: [5, 10, 25, 50]
          },
          total: 1
        };
      },
      computed: {
        sort: function sort() {
          return "desc" === this.sortation.order ? "-".concat(this.sortation.field) : this.sortation.field;
        },
        from: function from() {
          return this.pagination.perPage * (this.pagination.currentPage - 1);
        },
        to: function to() {
          var t = this.from + this.pagination.perPage;
          return this.total < t && (t = this.total), t;
        }
      },
      created: function created() {
        this.getList();
      },
      methods: {
        getList: function getList() {
          this.table = [{
            name: "Admin",
            email: "admin@material.com",
            created_at: "2020-01-01"
          }];
        },
        onProFeature: function onProFeature() {
          this.$store.dispatch("alerts/error", "This is a PRO feature.");
        },
        customSort: function customSort() {
          return !1;
        }
      }
    },
        Da = Aa,
        Ma = Object(d["a"])(Da, za, Ea, !1, null, null, null),
        La = Ma.exports,
        Ba = function Ba() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item md-medium-size-50 md-xsmall-size-100 md-size-25"
      }, [a("stats-card", {
        attrs: {
          "header-color": "blue"
        }
      }, [a("template", {
        slot: "header"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("i", {
        staticClass: "fab fa-twitter"
      })]), a("p", {
        staticClass: "category"
      }, [t._v("متابعون")]), a("h3", {
        staticClass: "title"
      }, [t._v(" +245 ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("update")]), t._v(" تم التحديث للتو ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-50 md-xsmall-size-100 md-size-25"
      }, [a("stats-card", {
        attrs: {
          "header-color": "rose"
        }
      }, [a("template", {
        slot: "header"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("equalizer")])], 1), a("p", {
        staticClass: "category"
      }, [t._v("الزيارات")]), a("h3", {
        staticClass: "title"
      }, [t._v(" 75.521 ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("local_offer")]), t._v(" تعقب ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-50 md-xsmall-size-100 md-size-25"
      }, [a("stats-card", {
        attrs: {
          "header-color": "green"
        }
      }, [a("template", {
        slot: "header"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("store")])], 1), a("p", {
        staticClass: "category"
      }, [t._v("إيرادات")]), a("h3", {
        staticClass: "title"
      }, [t._v(" $ 34.245 ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("date_range")]), t._v(" أخر 24 ساعه ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-50 md-xsmall-size-100 md-size-25"
      }, [a("stats-card", {
        attrs: {
          "header-color": "warning"
        }
      }, [a("template", {
        slot: "header"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("weekend")])], 1), a("p", {
        staticClass: "category"
      }, [t._v("الحجوزات")]), a("h3", {
        staticClass: "title"
      }, [t._v(" 184 ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", {
        staticClass: "text-danger"
      }, [t._v("warning")]), a("a", {
        attrs: {
          href: "#pablo"
        }
      }, [t._v("المزيد من المساحة")])], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-33"
      }, [a("chart-card", {
        attrs: {
          "chart-data": t.emailsSubscriptionChart.data,
          "chart-options": t.emailsSubscriptionChart.options,
          "chart-responsive-options": t.emailsSubscriptionChart.responsiveOptions,
          "chart-type": "Bar",
          "chart-inside-header": "",
          "background-color": "rose"
        }
      }, [a("template", {
        slot: "content"
      }, [a("h4", {
        staticClass: "title"
      }, [t._v("الآراء")]), a("p", {
        staticClass: "category"
      }, [t._v(" أداء الحملة ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("access_time")]), t._v(" قبل 10 دقيقة ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-33"
      }, [a("chart-card", {
        attrs: {
          "chart-data": t.dailySalesChart.data,
          "chart-options": t.dailySalesChart.options,
          "chart-type": "Line",
          "chart-inside-header": "",
          "background-color": "green"
        }
      }, [a("template", {
        slot: "content"
      }, [a("h4", {
        staticClass: "title"
      }, [t._v("المبيعات اليومية")]), a("p", {
        staticClass: "category"
      }, [a("span", {
        staticClass: "text-success"
      }, [a("i", {
        staticClass: "fas fa-long-arrow-alt-up"
      }), t._v(" 55% ")]), t._v(" زيادة ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("access_time")]), t._v(" قبل 4 دقيقة ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-33"
      }, [a("chart-card", {
        attrs: {
          "chart-data": t.dataCompletedTasksChart.data,
          "chart-options": t.dataCompletedTasksChart.options,
          "chart-type": "Line",
          "chart-inside-header": "",
          "background-color": "blue"
        }
      }, [a("template", {
        slot: "content"
      }, [a("h4", {
        staticClass: "title"
      }, [t._v("الآراء")]), a("p", {
        staticClass: "category"
      }, [t._v(" أداء الحملة ")])]), a("template", {
        slot: "footer"
      }, [a("div", {
        staticClass: "stats"
      }, [a("md-icon", [t._v("access_time")]), t._v(" قبل 24 دقيقة ")], 1)])], 2)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-50"
      }, [a("md-card", [a("md-card-header", {
        staticClass: "md-card-header-text md-card-header-warning"
      }, [a("div", {
        staticClass: "card-text"
      }, [a("h4", {
        staticClass: "title"
      }, [t._v("لتكاليف يبق")]), a("p", {
        staticClass: "category"
      }, [t._v(" بالإنزال وفي. خيار ومضى العمليات تم ذلك, تم معقل مرمى ")])])]), a("md-card-content", [a("md-table", {
        attrs: {
          "table-header-color": "orange"
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {}, [a("md-table-cell", {
              attrs: {
                "md-label": "وتم"
              }
            }, [t._v(t._s(s.id))]), a("md-table-cell", {
              attrs: {
                "md-label": "لأمريكية هذا"
              }
            }, [t._v(t._s(s.name))]), a("md-table-cell", {
              attrs: {
                "md-label": "شاسعالأمريكية"
              }
            }, [t._v(t._s(s.salary))]), a("md-table-cell", {
              attrs: {
                "md-label": "الأمريكية"
              }
            }, [t._v(t._s(s.country))])], 1);
          }
        }]),
        model: {
          value: t.users,
          callback: function callback(e) {
            t.users = e;
          },
          expression: "users"
        }
      })], 1)], 1)], 1), a("div", {
        staticClass: "md-layout-item md-medium-size-100 md-xsmall-size-100 md-size-50"
      }, [a("nav-tabs-card", [a("template", {
        slot: "content"
      }, [a("span", {
        staticClass: "md-nav-tabs-title"
      }, [t._v("منتصف:")]), a("md-tabs", {
        staticClass: "md-rose",
        attrs: {
          "md-sync-route": "",
          "md-alignment": "left"
        }
      }, [a("md-tab", {
        attrs: {
          id: "tab-home",
          "md-label": "ضرب",
          "md-icon": "bug_report"
        }
      }, [a("md-table", {
        on: {
          "md-selected": t.onSelect
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {
              attrs: {
                "md-selectable": "multiple",
                "md-auto-select": ""
              }
            }, [a("md-table-cell", [t._v(t._s(s.tab))]), a("md-table-cell", [a("md-button", {
              staticClass: "md-just-icon md-simple md-primary"
            }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Edit")])], 1), a("md-button", {
              staticClass: "md-just-icon md-simple md-danger"
            }, [a("md-icon", [t._v("close")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Close")])], 1)], 1)], 1);
          }
        }]),
        model: {
          value: t.firstTabs,
          callback: function callback(e) {
            t.firstTabs = e;
          },
          expression: "firstTabs"
        }
      })], 1), a("md-tab", {
        attrs: {
          id: "tab-pages",
          "md-label": "السفن",
          "md-icon": "code"
        }
      }, [a("md-table", {
        on: {
          "md-selected": t.onSelect
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {
              attrs: {
                "md-selectable": "multiple",
                "md-auto-select": ""
              }
            }, [a("md-table-cell", [t._v(t._s(s.tab))]), a("md-table-cell", [a("md-button", {
              staticClass: "md-just-icon md-simple md-primary"
            }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Edit")])], 1), a("md-button", {
              staticClass: "md-just-icon md-simple md-danger"
            }, [a("md-icon", [t._v("close")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Close")])], 1)], 1)], 1);
          }
        }]),
        model: {
          value: t.secondTabs,
          callback: function callback(e) {
            t.secondTabs = e;
          },
          expression: "secondTabs"
        }
      })], 1), a("md-tab", {
        attrs: {
          id: "tab-posts",
          "md-label": "فصل.",
          "md-icon": "cloud"
        }
      }, [a("md-table", {
        on: {
          "md-selected": t.onSelect
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {
              attrs: {
                "md-selectable": "multiple",
                "md-auto-select": ""
              }
            }, [a("md-table-cell", [t._v(t._s(s.tab))]), a("md-table-cell", [a("md-button", {
              staticClass: "md-just-icon md-simple md-primary"
            }, [a("md-icon", [t._v("edit")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Edit")])], 1), a("md-button", {
              staticClass: "md-just-icon md-simple md-danger"
            }, [a("md-icon", [t._v("close")]), a("md-tooltip", {
              attrs: {
                "md-direction": "top"
              }
            }, [t._v("Close")])], 1)], 1)], 1);
          }
        }]),
        model: {
          value: t.thirdTabs,
          callback: function callback(e) {
            t.thirdTabs = e;
          },
          expression: "thirdTabs"
        }
      })], 1)], 1)], 1)], 2)], 1)]);
    },
        Na = [],
        Ra = {
      components: {
        StatsCard: Ie,
        ChartCard: Le,
        NavTabsCard: He
      },
      props: {
        profileCard: {
          type: String,
          "default": "http://localhost:8080//img/faces/card-profile1-square.jpg"
        }
      },
      data: function data() {
        return {
          selected: [],
          firstTabs: [{
            tab: " فقد لمحاكم الاندونيسية, بلاده بالتوقيع تم يبق. جعل السبب وفرنسا الصينية أي."
          }, {
            tab: "بحث. كل مما ٢٠٠٤ شاسعة العسكري جعل السبب وفرنسا الصينية أي."
          }, {
            tab: "تسبب أفريقيا ضرب عن, عن إنطلاق جعل السبب وفرنسا الصينية أي."
          }],
          secondTabs: [{
            tab: "قدما مليون بين عن, مرجع منتصف الأمريكية  جعل السبب وفرنسا الصينية أي."
          }, {
            tab: "قدما مليون بين عن, مرجع منتصف الأمريكية  جعل السبب وفرنسا الصينية أي."
          }, {
            tab: "قدما مليون بين عن, مرجع منتصف الأمريكية  جعل السبب وفرنسا الصينية أي."
          }],
          thirdTabs: [{
            tab: "قدما مليون بين عن, مرجع منتصف الأمريكية  جعل السبب وفرنسا الصينية أي."
          }, {
            tab: "قدما مليون بين عن, مرجع منتصف الأمريكية  جعل السبب وفرنسا الصينية أي."
          }, {
            tab: "قدما مليون بين عن, مرجع منتصف الأمريكية  جعل السبب وفرنسا الصينية أي."
          }],
          users: [{
            id: 1,
            name: "السبب وفرنسا الصينية ",
            salary: "$36,738",
            country: "تكاليف "
          }, {
            id: 2,
            name: "بمباركة بها",
            salary: "$23,738",
            country: "الأمريكية من"
          }, {
            id: 3,
            name: "شاسعالأمريكية",
            salary: "$56,142",
            country: "السفن وعُرفت"
          }, {
            id: 4,
            name: "الاندونيسية",
            salary: "$38,735",
            country: "فصل."
          }],
          dailySalesChart: {
            data: {
              labels: ["M", "T", "W", "T", "F", "S", "S"],
              series: [[12, 17, 7, 17, 23, 18, 38]]
            },
            options: {
              lineSmooth: this.$Chartist.Interpolation.cardinal({
                tension: 0
              }),
              low: 0,
              high: 50,
              chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
              }
            }
          },
          dataCompletedTasksChart: {
            data: {
              labels: ["12am", "3pm", "6pm", "9pm", "12pm", "3am", "6am", "9am"],
              series: [[230, 750, 450, 300, 280, 240, 200, 190]]
            },
            options: {
              lineSmooth: this.$Chartist.Interpolation.cardinal({
                tension: 0
              }),
              low: 0,
              high: 1e3,
              chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
              }
            }
          },
          emailsSubscriptionChart: {
            data: {
              labels: ["Ja", "Fe", "Ma", "Ap", "Mai", "Ju", "Jul", "Au", "Se", "Oc", "No", "De"],
              series: [[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]]
            },
            options: {
              axisX: {
                showGrid: !1
              },
              low: 0,
              high: 1e3,
              chartPadding: {
                top: 0,
                right: 5,
                bottom: 0,
                left: 0
              }
            },
            responsiveOptions: [["screen and (max-width: 640px)", {
              seriesBarDistance: 5,
              axisX: {
                labelInterpolationFnc: function labelInterpolationFnc(t) {
                  return t[0];
                }
              }
            }]]
          }
        };
      },
      methods: {
        onSelect: function onSelect(t) {
          this.selected = t;
        }
      }
    },
        Va = Ra,
        Ua = Object(d["a"])(Va, Ba, Na, !1, null, null, null),
        Ha = Ua.exports,
        Fa = function Fa() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "md-layout text-center login-fix-page"
      }, [t._m(0), a("div", {
        staticClass: "md-layout-item md-size-33 md-medium-size-50 md-small-size-70 md-xsmall-size-100"
      }, [a("form", {
        on: {
          submit: function submit(e) {
            return e.preventDefault(), t.login(e);
          }
        }
      }, [a("login-card", {
        attrs: {
          "header-color": "green"
        }
      }, [a("h4", {
        staticClass: "title",
        attrs: {
          slot: "title"
        },
        slot: "title"
      }, [t._v("Log in")]), a("md-button", {
        ref: "#facebook",
        staticClass: "md-just-icon md-simple md-white",
        attrs: {
          slot: "buttons"
        },
        slot: "buttons"
      }, [a("i", {
        staticClass: "fab fa-facebook-square"
      })]), a("md-button", {
        staticClass: "md-just-icon md-simple md-white",
        attrs: {
          slot: "buttons",
          href: "#twitter"
        },
        slot: "buttons"
      }, [a("i", {
        staticClass: "fab fa-twitter"
      })]), a("md-button", {
        staticClass: "md-just-icon md-simple md-white",
        attrs: {
          slot: "buttons",
          href: "#google"
        },
        slot: "buttons"
      }, [a("i", {
        staticClass: "fab fa-google-plus-g"
      })]), a("p", {
        staticClass: "description",
        attrs: {
          slot: "description"
        },
        slot: "description"
      }, [t._v("Or Be Classical")]), a("md-field", {
        staticClass: "form-group md-invalid",
        staticStyle: {
          "margin-bottom": "28px"
        },
        attrs: {
          slot: "inputs"
        },
        slot: "inputs"
      }, [a("md-icon", [t._v("email")]), a("label", [t._v("Email...")]), a("md-input", {
        attrs: {
          type: "email"
        },
        model: {
          value: t.email,
          callback: function callback(e) {
            t.email = e;
          },
          expression: "email"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.email
        }
      })], 1), a("md-field", {
        staticClass: "form-group md-invalid",
        attrs: {
          slot: "inputs"
        },
        slot: "inputs"
      }, [a("md-icon", [t._v("lock_outline")]), a("label", [t._v("Password...")]), a("md-input", {
        attrs: {
          type: "password"
        },
        model: {
          value: t.password,
          callback: function callback(e) {
            t.password = e;
          },
          expression: "password"
        }
      })], 1), a("md-button", {
        staticClass: "md-simple md-success md-lg",
        attrs: {
          slot: "footer",
          type: "submit"
        },
        slot: "footer"
      }, [t._v(" Lets Go ")])], 1)], 1)])]);
    },
        Ka = [function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("div", {
        staticClass: "text-center"
      }, [a("h3", [t._v("Log in to Vue Material Dashboard Laravel Live Preview")]), a("p", [t._v(" Log in to see how you can go from frontend to fullstack in an instant with an API-based Laravel backend. ")])]), a("div", {
        staticClass: "text-center",
        staticStyle: {
          "margin-bottom": "32px"
        }
      }, [a("h5", [a("strong", [t._v("You can log in with:")])]), a("div", [t._v("Username "), a("b", [t._v("admin@material.com")]), t._v(" Password "), a("b", [t._v("secret")])])])]);
    }],
        Wa = {
      components: {
        LoginCard: Se,
        ValidationError: Ge
      },
      mixins: [ma],
      computed: {
        isAuthenticated: function isAuthenticated() {
          return this.$store.getters.isAuthenticated();
        }
      },
      data: function data() {
        return {
          email: "admin@material.com",
          password: "secret"
        };
      },
      methods: {
        login: function login() {
          var t = this;
          return Object(Ht["a"])(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function e() {
            var a, s;
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function (e) {
              while (1) {
                switch (e.prev = e.next) {
                  case 0:
                    return a = {
                      data: {
                        type: "token",
                        attributes: {
                          email: t.email,
                          password: t.password
                        }
                      }
                    }, s = {
                      headers: {
                        Accept: "application/vnd.api+json",
                        "Content-Type": "application/vnd.api+json"
                      }
                    }, e.prev = 2, e.next = 5, t.$store.dispatch("login", {
                      user: a,
                      requestOptions: s
                    });

                  case 5:
                    e.next = 12;
                    break;

                  case 7:
                    return e.prev = 7, e.t0 = e["catch"](2), e.next = 11, t.$store.dispatch("alerts/error", "Invalid credentials!");

                  case 11:
                    t.setApiValidation(e.t0.response.data.errors);

                  case 12:
                  case "end":
                    return e.stop();
                }
              }
            }, e, null, [[2, 7]]);
          }))();
        }
      }
    },
        Ya = Wa,
        qa = (a("f2eb"), Object(d["a"])(Ya, Fa, Ka, !1, null, "577ed9f5", null)),
        Ga = qa.exports,
        Ja = function Ja() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item"
      }, [a("form", {
        on: {
          submit: function submit(e) {
            return e.preventDefault(), t.register(e);
          }
        }
      }, [a("signup-card", [a("h2", {
        staticClass: "title text-center",
        attrs: {
          slot: "title"
        },
        slot: "title"
      }, [t._v("Register")]), a("div", {
        staticClass: "md-layout-item md-size-50 md-medium-size-50 md-small-size-100 ml-auto",
        attrs: {
          slot: "content-left"
        },
        slot: "content-left"
      }, t._l(t.contentLeft, function (e) {
        return a("div", {
          key: e.title,
          staticClass: "info info-horizontal"
        }, [a("div", {
          "class": "icon " + e.colorIcon
        }, [a("md-icon", [t._v(t._s(e.icon))])], 1), a("div", {
          staticClass: "description"
        }, [a("h4", {
          staticClass: "info-title"
        }, [t._v(t._s(e.title))]), a("p", {
          staticClass: "description"
        }, [t._v(" " + t._s(e.description) + " ")])])]);
      }), 0), a("div", {
        staticClass: "md-layout-item md-size-50 md-medium-size-50 md-small-size-100 mr-auto",
        attrs: {
          slot: "content-right"
        },
        slot: "content-right"
      }, [a("div", {
        staticClass: "social-line text-center"
      }, [a("md-button", {
        staticClass: "md-just-icon md-round md-twitter"
      }, [a("i", {
        staticClass: "fab fa-twitter"
      })]), a("md-button", {
        staticClass: "md-just-icon md-round md-dribbble"
      }, [a("i", {
        staticClass: "fab fa-dribbble"
      })]), a("md-button", {
        staticClass: "md-just-icon md-round md-facebook"
      }, [a("i", {
        staticClass: "fab fa-facebook-f"
      })]), a("h4", {
        staticClass: "mt-3"
      }, [t._v("or be classical")])], 1), a("md-field", {
        staticClass: "md-form-group md-invalid",
        staticStyle: {
          "margin-bottom": "2rem"
        }
      }, [a("md-icon", [t._v("face")]), a("label", [t._v("Name")]), a("md-input", {
        model: {
          value: t.name,
          callback: function callback(e) {
            t.name = e;
          },
          expression: "name"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.name
        }
      })], 1), a("md-field", {
        staticClass: "md-form-group md-invalid",
        staticStyle: {
          "margin-bottom": "2rem"
        }
      }, [a("md-icon", [t._v("email")]), a("label", [t._v("Email")]), a("md-input", {
        model: {
          value: t.email,
          callback: function callback(e) {
            t.email = e;
          },
          expression: "email"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.email
        }
      })], 1), a("md-field", {
        staticClass: "md-form-group md-invalid",
        staticStyle: {
          "margin-bottom": "2rem"
        }
      }, [a("md-icon", [t._v("developer_mode")]), a("label", [t._v("Developer Type")]), a("md-input", {
        model: {
          value: t.developer,
          callback: function callback(e) {
            t.developer = e;
          },
          expression: "developer"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.developer
        }
      })], 1), a("md-datepicker", {
        model: {
          value: t.selectedDate,
          callback: function callback(e) {
            t.selectedDate = e;
          },
          expression: "selectedDate"
        }
      }), a("md-field", {
        staticClass: "md-form-group md-invalid",
        staticStyle: {
          "margin-bottom": "2rem"
        }
      }, [a("label", [t._v("Address")]), a("md-textarea", {
        model: {
          value: t.address,
          callback: function callback(e) {
            t.address = e;
          },
          expression: "address"
        }
      }), a("md-icon", [t._v("description")])], 1), a("md-field", {
        staticClass: "md-form-group md-invalid",
        staticStyle: {
          "margin-bottom": "2rem"
        }
      }, [a("md-icon", [t._v("email")]), a("label", [t._v("Qualification")]), a("md-input", {
        model: {
          value: t.qualification,
          callback: function callback(e) {
            t.qualification = e;
          },
          expression: "qualification"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.name
        }
      })], 1), a("md-field", {
        staticClass: "md-form-group md-invalid",
        staticStyle: {
          "margin-bottom": "2rem"
        }
      }, [a("md-icon", [t._v("email")]), a("label", [t._v("Skills")]), a("md-input", {
        model: {
          value: t.skills,
          callback: function callback(e) {
            t.skills = e;
          },
          expression: "skills"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.name
        }
      })], 1), a("md-field", {
        staticClass: "md-form-group md-invalid",
        staticStyle: {
          "margin-bottom": "2rem"
        }
      }, [a("md-icon", [t._v("email")]), a("label", [t._v("Experience")]), a("md-input", {
        model: {
          value: t.experience,
          callback: function callback(e) {
            t.experience = e;
          },
          expression: "experience"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.name
        }
      })], 1), a("md-field", {
        staticClass: "md-form-group md-invalid",
        staticStyle: {
          "margin-bottom": "2rem"
        }
      }, [a("md-icon", [t._v("lock_outline")]), a("label", [t._v("Password")]), a("md-input", {
        attrs: {
          type: "password"
        },
        model: {
          value: t.password,
          callback: function callback(e) {
            t.password = e;
          },
          expression: "password"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.password
        }
      })], 1), a("md-field", {
        staticClass: "md-form-group md-invalid"
      }, [a("md-icon", [t._v("lock_outline")]), a("label", [t._v("Confirm Password")]), a("md-input", {
        attrs: {
          type: "password"
        },
        model: {
          value: t.password_confirmation,
          callback: function callback(e) {
            t.password_confirmation = e;
          },
          expression: "password_confirmation"
        }
      }), a("validation-error", {
        attrs: {
          errors: t.apiValidationErrors.password_confirmation
        }
      })], 1), a("md-checkbox", {
        model: {
          value: t["boolean"],
          callback: function callback(e) {
            t["boolean"] = e;
          },
          expression: "boolean"
        }
      }, [t._v("I agree to the "), a("a", [t._v("terms and conditions")]), t._v(".")]), a("div", {
        staticClass: "button-container"
      }, [a("md-button", {
        staticClass: "md-success md-round mt-4",
        attrs: {
          slot: "footer",
          type: "submit"
        },
        slot: "footer"
      }, [t._v(" Get Started ")])], 1)], 1)])], 1)])]);
    },
        Xa = [],
        Qa = {
      components: {
        SignupCard: _e,
        ValidationError: Ge
      },
      mixins: [ma],
      data: function data() {
        return {
          name: null,
          "boolean": !1,
          email: null,
          role: 1,
          password: null,
          developer: "",
          password_confirmation: null,
          address: "",
          qualification: "",
          skills: "",
          experience: "",
          selectedDate: "",
          contentLeft: [{
            colorIcon: "icon-success",
            icon: "timeline",
            title: "Marketing",
            description: "We've created the marketing campaign of the website. It was a very interesting collaboration."
          }, {
            colorIcon: "icon-danger",
            icon: "code",
            title: "Fully Coded in HTML5",
            description: "We've developed the website with HTML5 and CSS3. The client has access to the code using GitHub."
          }, {
            colorIcon: "icon-info",
            icon: "group",
            title: "Built Audience",
            description: "There is also a Fully Customizable CMS Admin Dashboard for this product."
          }]
        };
      },
      methods: {
        register: function register() {
          var t = this;
          return Object(Ht["a"])(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function e() {
            var a, s;
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function (e) {
              while (1) {
                switch (e.prev = e.next) {
                  case 0:
                    if (t["boolean"]) {
                      e.next = 4;
                      break;
                    }

                    return e.next = 3, t.$store.dispatch("alerts/error", "You need to agree with our terms and conditions.");

                  case 3:
                    return e.abrupt("return");

                  case 4:
                    return a = {
                      data: {
                        type: "token",
                        attributes: {
                          name: t.name,
                          email: t.email,
                          password: t.password,
                          password_confirmation: t.password_confirmation
                        }
                      }
                    }, s = {
                      headers: {
                        Accept: "application/vnd.api+json",
                        "Content-Type": "application/vnd.api+json"
                      }
                    }, e.prev = 6, e.next = 9, t.$store.dispatch("register", {
                      user: a,
                      requestOptions: s
                    });

                  case 9:
                    return e.next = 11, t.$store.dispatch("alerts/success", "Successfully registered.");

                  case 11:
                    e.next = 18;
                    break;

                  case 13:
                    return e.prev = 13, e.t0 = e["catch"](6), e.next = 17, t.$store.dispatch("alerts/error", "Oops, something went wrong");

                  case 17:
                    t.setApiValidation(e.t0.response.data.errors);

                  case 18:
                  case "end":
                    return e.stop();
                }
              }
            }, e, null, [[6, 13]]);
          }))();
        }
      }
    },
        Za = Qa,
        ts = Object(d["a"])(Za, Ja, Xa, !1, null, null, null),
        es = ts.exports,
        as = function as() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", [t._m(0), a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item md-size-50 md-small-size-100"
      }, [a("md-card", [a("md-card-header", [a("h4", {
        staticClass: "title"
      }, [t._v("Notifications Style")])]), a("md-card-content", [a("div", {
        staticClass: "alert alert-info"
      }, [a("span", [t._v("This is a plain notification")])]), a("div", {
        staticClass: "alert alert-info"
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("span", [t._v("This is a notification with close button.")])]), a("div", {
        staticClass: "alert alert-info alert-with-icon",
        attrs: {
          "data-notify": "container"
        }
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("i", {
        staticClass: "material-icons",
        attrs: {
          "data-notify": "icon"
        }
      }, [t._v("add_alert")]), a("span", {
        attrs: {
          "data-notify": "message"
        }
      }, [t._v("This is a notification with close button and icon and have many lines. You can see that the icon and the close button are always vertically aligned. This is a beautiful notification. So you don't have to worry about the style.")])]), a("div", {
        staticClass: "alert alert-rose alert-with-icon",
        attrs: {
          "data-notify": "container"
        }
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("i", {
        staticClass: "material-icons",
        attrs: {
          "data-notify": "icon"
        }
      }, [t._v("add_alert")]), a("span", {
        attrs: {
          "data-notify": "message"
        }
      }, [t._v('This is a notification with close button and icon and is made with ".alert-rose". You can see that the icon and the close button are always vertically aligned. This is a beautiful notification. So you don\'t have to worry about the style.')])])])], 1)], 1), a("div", {
        staticClass: "md-layout-item md-size-50 md-small-size-100"
      }, [a("md-card", [a("md-card-header", [a("h4", {
        staticClass: "title"
      }, [t._v("Notification states")])]), a("md-card-content", [a("div", {
        staticClass: "alert alert-info"
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("span", [a("b", [t._v(" Info - ")]), t._v(' This is a regular notification made with ".alert-info"')])]), a("div", {
        staticClass: "alert alert-success"
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("span", [a("b", [t._v(" Success - ")]), t._v(' This is a regular notification made with ".alert-success"')])]), a("div", {
        staticClass: "alert alert-warning"
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("span", [a("b", [t._v(" Warning - ")]), t._v(' This is a regular notification made with ".alert-warning"')])]), a("div", {
        staticClass: "alert alert-danger"
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("span", [a("b", [t._v(" Danger - ")]), t._v(' This is a regular notification made with ".alert-danger"')])]), a("div", {
        staticClass: "alert alert-primary"
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("span", [a("b", [t._v(" Primary - ")]), t._v(' This is a regular notification made with ".alert-primary"')])]), a("div", {
        staticClass: "alert alert-rose"
      }, [a("button", {
        staticClass: "close",
        attrs: {
          type: "button",
          "aria-hidden": "true"
        }
      }, [t._v("×")]), a("span", [a("b", [t._v(" Rose - ")]), t._v(' This is a regular notification made with ".alert-rose"')])])])], 1)], 1), a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("md-card", [a("md-card-header", [a("h4", {
        staticClass: "title text-center"
      }, [t._v(" Notifications Places "), a("p", {
        staticClass: "category"
      }, [t._v("Click to view notifications")])])]), a("md-card-content", [a("div", {
        staticClass: "places-buttons text-center"
      }, [a("md-button", {
        staticClass: "md-success",
        on: {
          click: function click(e) {
            return t.notifyVue("top", "left");
          }
        }
      }, [t._v("Top Left")]), a("md-button", {
        staticClass: "md-success",
        on: {
          click: function click(e) {
            return t.notifyVue("top", "center");
          }
        }
      }, [t._v("Top Center")]), a("md-button", {
        staticClass: "md-success",
        on: {
          click: function click(e) {
            return t.notifyVue("top", "right");
          }
        }
      }, [t._v("Top Right")]), a("md-button", {
        staticClass: "md-success",
        on: {
          click: function click(e) {
            return t.notifyVue("bottom", "left");
          }
        }
      }, [t._v("Bottom Left")]), a("md-button", {
        staticClass: "md-success",
        on: {
          click: function click(e) {
            return t.notifyVue("bottom", "center");
          }
        }
      }, [t._v("Bottom Center")]), a("md-button", {
        staticClass: "md-success",
        on: {
          click: function click(e) {
            return t.notifyVue("bottom", "right");
          }
        }
      }, [t._v("Bottom Right")])], 1)])], 1)], 1)])]);
    },
        ss = [function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "header text-center"
      }, [a("h3", {
        staticClass: "title"
      }, [t._v("Notifications")]), a("p", {
        staticClass: "category"
      }, [t._v(" Handcrafted by our friend "), a("a", {
        attrs: {
          target: "_blank",
          href: "https://github.com/mouse0270"
        }
      }, [t._v("Robert McIntosh")]), t._v(". Please checkout the "), a("a", {
        attrs: {
          href: "https://bootstrap-notify.remabledesigns.com/",
          target: "_blank"
        }
      }, [t._v("full documentation.")])])]);
    }],
        is = {
      props: {
        registerImg: {
          type: String,
          "default": "http://localhost:8080//img/card-1.jpg"
        },
        applyImg: {
          type: String,
          "default": "http://localhost:8080//img/card-2.jpg"
        }
      },
      data: function data() {
        return {
          type: ["", "info", "success", "warning", "danger"],
          notifications: {
            topCenter: !1
          }
        };
      },
      methods: {
        notifyVue: function notifyVue(t, e) {
          var a = Math.floor(4 * Math.random() + 1);
          this.$notify({
            timeout: 2500,
            message: "Welcome to <b>Vue Material Dashboard Pro</b> - a beautiful admin panel for every web developer.",
            icon: "add_alert",
            horizontalAlign: e,
            verticalAlign: t,
            type: this.type[a]
          });
        }
      }
    },
        ns = is,
        rs = (a("3f6f"), Object(d["a"])(ns, as, ss, !1, null, null, null)),
        os = rs.exports,
        ls = function ls() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", [t._m(0), a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item"
      }, [a("md-card", {
        staticClass: "md-card-plain"
      }, [a("md-card-content", [a("div", {
        staticClass: "iframe-container hidden-sm"
      }, [a("iframe", {
        attrs: {
          src: "https://vuematerial.io/components/icon"
        }
      }, [a("p", [t._v("Your browser does not support iframes.")])])]), a("div", {
        staticClass: "hidden-md"
      }, [a("h5", [t._v(" The icons are visible on Desktop mode inside an iframe. Since the iframe is not working on Mobile and Tablets please visit the icons on their original page on Google. Check the "), a("a", {
        attrs: {
          href: "https://design.google.com/icons/",
          target: "_blank"
        }
      }, [t._v("Material Icons")])])])])], 1)], 1)])]);
    },
        cs = [function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "header text-center"
      }, [a("h3", {
        staticClass: "title"
      }, [t._v("Material Design Icons")]), a("p", {
        staticClass: "category"
      }, [t._v(" Handcrafted by our friends from "), a("a", {
        attrs: {
          target: "_blank",
          href: "https://design.google.com/icons/"
        }
      }, [t._v("Google")])])]);
    }],
        ds = {},
        ms = ds,
        us = Object(d["a"])(ms, ls, cs, !1, null, null, null),
        ps = us.exports,
        hs = function hs() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", [t._m(0), a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item"
      }, [a("md-card", [a("md-card-content", [a("div", {
        attrs: {
          id: "typography"
        }
      }, [a("div", {
        staticClass: "title"
      }, [a("h2", [t._v("Typography")])]), a("div", {
        staticClass: "row"
      }, [a("div", {
        staticClass: "tim-typo"
      }, [a("h1", [a("span", {
        staticClass: "tim-note"
      }, [t._v("Header 1")]), t._v("The Life of Material Dashboard ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("h2", [a("span", {
        staticClass: "tim-note"
      }, [t._v("Header 2")]), t._v("The life of Material Dashboard ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("h3", [a("span", {
        staticClass: "tim-note"
      }, [t._v("Header 3")]), t._v("The life of Material Dashboard ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("h4", [a("span", {
        staticClass: "tim-note"
      }, [t._v("Header 4")]), t._v("The life of Material Dashboard ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("h5", [a("span", {
        staticClass: "tim-note"
      }, [t._v("Header 5")]), t._v("The life of Material Dashboard ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("h6", [a("span", {
        staticClass: "tim-note"
      }, [t._v("Header 6")]), t._v("The life of Material Dashboard ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("p", [a("span", {
        staticClass: "tim-note"
      }, [t._v("Paragraph")]), t._v(" I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("span", {
        staticClass: "tim-note"
      }, [t._v("Quote")]), a("blockquote", [a("p", [t._v(" I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at. ")]), a("small", [t._v(" Kanye West, Musician ")])])]), a("div", {
        staticClass: "tim-typo"
      }, [a("span", {
        staticClass: "tim-note"
      }, [t._v("Muted Text")]), a("p", {
        staticClass: "text-muted"
      }, [t._v(" I will be the leader of a company that ends up being worth billions of dollars, because I got the answers... ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("span", {
        staticClass: "tim-note"
      }, [t._v("Primary Text")]), a("p", {
        staticClass: "text-primary"
      }, [t._v(" I will be the leader of a company that ends up being worth billions of dollars, because I got the answers... ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("span", {
        staticClass: "tim-note"
      }, [t._v("Info Text")]), a("p", {
        staticClass: "text-info"
      }, [t._v(" I will be the leader of a company that ends up being worth billions of dollars, because I got the answers... ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("span", {
        staticClass: "tim-note"
      }, [t._v("Success Text")]), a("p", {
        staticClass: "text-success"
      }, [t._v(" I will be the leader of a company that ends up being worth billions of dollars, because I got the answers... ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("span", {
        staticClass: "tim-note"
      }, [t._v("Warning Text")]), a("p", {
        staticClass: "text-warning"
      }, [t._v(" I will be the leader of a company that ends up being worth billions of dollars, because I got the answers... ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("span", {
        staticClass: "tim-note"
      }, [t._v("Danger Text")]), a("p", {
        staticClass: "text-danger"
      }, [t._v(" I will be the leader of a company that ends up being worth billions of dollars, because I got the answers... ")])]), a("div", {
        staticClass: "tim-typo"
      }, [a("h2", [a("span", {
        staticClass: "tim-note"
      }, [t._v("Small Tag")]), t._v(" Header with small subtitle "), a("br"), a("small", [t._v('Use "small" tag for the headers')])])])])])])], 1)], 1)])]);
    },
        fs = [function () {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "header text-center"
      }, [a("h3", {
        staticClass: "title"
      }, [t._v("Material Dashboard Heading")]), a("p", {
        staticClass: "category"
      }, [t._v("Created using Roboto Font Family")])]);
    }],
        vs = {},
        bs = vs,
        gs = Object(d["a"])(bs, hs, fs, !1, null, null, null),
        _s = gs.exports,
        Cs = function Cs() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        staticClass: "md-layout"
      }, [a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("md-card", [a("md-card-header", {
        staticClass: "md-card-header-icon md-card-header-green"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("assignment")])], 1), a("h4", {
        staticClass: "title"
      }, [t._v("Simple Table")])]), a("md-card-content", [a("md-table", {
        attrs: {
          "table-header-color": "green"
        },
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {}, [a("md-table-cell", {
              attrs: {
                "md-label": "ID"
              }
            }, [t._v(t._s(s.id))]), a("md-table-cell", {
              attrs: {
                "md-label": "Name"
              }
            }, [t._v(t._s(s.name))]), a("md-table-cell", {
              attrs: {
                "md-label": "Country"
              }
            }, [t._v(t._s(s.country))]), a("md-table-cell", {
              attrs: {
                "md-label": "City"
              }
            }, [t._v(t._s(s.city))]), a("md-table-cell", {
              attrs: {
                "md-label": "Salary"
              }
            }, [t._v(t._s(s.salary))])], 1);
          }
        }]),
        model: {
          value: t.tableData,
          callback: function callback(e) {
            t.tableData = e;
          },
          expression: "tableData"
        }
      })], 1)], 1)], 1), a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("md-card", {
        staticClass: "md-card-plain"
      }, [a("md-card-header", {
        staticClass: "md-card-header-icon md-card-header-green"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("assignment")])], 1), a("h4", {
        staticClass: "title mt-0"
      }, [t._v("Table on Plain Background")]), a("p", {
        staticClass: "category"
      }, [t._v("Here is a subtitle for this table")])]), a("md-card-content", [a("md-table", {
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {}, [a("md-table-cell", {
              attrs: {
                "md-label": "ID"
              }
            }, [t._v(t._s(s.id))]), a("md-table-cell", {
              attrs: {
                "md-label": "Name"
              }
            }, [t._v(t._s(s.name))]), a("md-table-cell", {
              attrs: {
                "md-label": "Country"
              }
            }, [t._v(t._s(s.country))]), a("md-table-cell", {
              attrs: {
                "md-label": "City"
              }
            }, [t._v(t._s(s.city))]), a("md-table-cell", {
              attrs: {
                "md-label": "Salary"
              }
            }, [t._v(t._s(s.salary))])], 1);
          }
        }]),
        model: {
          value: t.tableDataPlain,
          callback: function callback(e) {
            t.tableDataPlain = e;
          },
          expression: "tableDataPlain"
        }
      })], 1)], 1)], 1), a("div", {
        staticClass: "md-layout-item md-size-100"
      }, [a("md-card", [a("md-card-header", {
        staticClass: "md-card-header-icon md-card-header-green"
      }, [a("div", {
        staticClass: "card-icon"
      }, [a("md-icon", [t._v("assignment")])], 1), a("h4", {
        staticClass: "title"
      }, [t._v("Regular Table with Colors")])]), a("md-card-content", [a("md-table", {
        staticClass: "table-full-width",
        scopedSlots: t._u([{
          key: "md-table-row",
          fn: function fn(e) {
            var s = e.item;
            return a("md-table-row", {
              "class": t.getClass(s)
            }, [a("md-table-cell", {
              attrs: {
                "md-label": "ID"
              }
            }, [t._v(t._s(s.id))]), a("md-table-cell", {
              attrs: {
                "md-label": "Name"
              }
            }, [t._v(t._s(s.name))]), a("md-table-cell", {
              attrs: {
                "md-label": "Country"
              }
            }, [t._v(t._s(s.country))]), a("md-table-cell", {
              attrs: {
                "md-label": "City"
              }
            }, [t._v(t._s(s.city))]), a("md-table-cell", {
              attrs: {
                "md-label": "Salary"
              }
            }, [t._v(t._s(s.salary))])], 1);
          }
        }]),
        model: {
          value: t.tableDataColor,
          callback: function callback(e) {
            t.tableDataColor = e;
          },
          expression: "tableDataColor"
        }
      })], 1)], 1)], 1)]);
    },
        ys = [],
        ws = {
      data: function data() {
        return {
          selected: [],
          tableData: [{
            id: 1,
            name: "Dakota Rice",
            salary: "$36.738",
            country: "Niger",
            city: "Oud-Turnhout"
          }, {
            id: 2,
            name: "Minerva Hooper",
            salary: "$23,789",
            country: "Curaçao",
            city: "Sinaai-Waas"
          }, {
            id: 3,
            name: "Sage Rodriguez",
            salary: "$56,142",
            country: "Netherlands",
            city: "Baileux"
          }, {
            id: 4,
            name: "Philip Chaney",
            salary: "$38,735",
            country: "Korea, South",
            city: "Overland Park"
          }, {
            id: 5,
            name: "Doris Greene",
            salary: "$63,542",
            country: "Malawi",
            city: "Feldkirchen in Kärnten"
          }],
          tableDataPlain: [{
            id: 1,
            name: "Dakota Rice",
            salary: "$36.738",
            country: "Niger",
            city: "Oud-Turnhout"
          }, {
            id: 2,
            name: "Minerva Hooper",
            salary: "$23,789",
            country: "Curaçao",
            city: "Sinaai-Waas"
          }, {
            id: 3,
            name: "Sage Rodriguez",
            salary: "$56,142",
            country: "Netherlands",
            city: "Baileux"
          }, {
            id: 4,
            name: "Philip Chaney",
            salary: "$38,735",
            country: "Korea, South",
            city: "Overland Park"
          }, {
            id: 5,
            name: "Doris Greene",
            salary: "$63,542",
            country: "Malawi",
            city: "Feldkirchen in Kärnten"
          }],
          tableDataColor: [{
            id: 1,
            name: "Dakota Rice",
            salary: "$36.738",
            country: "Niger",
            city: "Oud-Turnhout"
          }, {
            id: 2,
            name: "Minerva Hooper",
            salary: "$23,789",
            country: "Curaçao",
            city: "Sinaai-Waas"
          }, {
            id: 3,
            name: "Sage Rodriguez",
            salary: "$56,142",
            country: "Netherlands",
            city: "Baileux"
          }, {
            id: 4,
            name: "Philip Chaney",
            salary: "$38,735",
            country: "Korea, South",
            city: "Overland Park"
          }, {
            id: 5,
            name: "Philip Chaney",
            salary: "$38,735",
            country: "Korea, South",
            city: "Overland Park"
          }, {
            id: 6,
            name: "Philip Chaney",
            salary: "$38,735",
            country: "Korea, South",
            city: "Overland Park"
          }, {
            id: 7,
            name: "Doris Greene",
            salary: "$63,542",
            country: "Malawi",
            city: "Feldkirchen in Kärnten"
          }]
        };
      },
      methods: {
        getClass: function getClass(t) {
          var e = t.id;
          return {
            "table-success": 1 === e,
            "table-info": 3 === e,
            "table-danger": 5 === e,
            "table-warning": 7 === e
          };
        }
      }
    },
        ks = ws,
        xs = (a("fce4"), Object(d["a"])(ks, Cs, ys, !1, null, "1f413d9d", null)),
        Ss = xs.exports,
        $s = function $s() {
      var t = this,
          e = t.$createElement,
          a = t._self._c || e;
      return a("div", {
        attrs: {
          id: "map"
        }
      });
    },
        Ts = [],
        Ps = "AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU",
        js = a("6344"),
        Os = new js["Loader"](Ps),
        Is = {
      data: function data() {
        return {
          nav: null
        };
      },
      mounted: function mounted() {
        Os.load().then(function (t) {
          var e = new t.maps.LatLng(40.748817, -73.985428),
              a = {
            zoom: 13,
            center: e,
            scrollwheel: !1,
            disableDefaultUI: !0,
            zoomControl: !0,
            styles: [{
              featureType: "water",
              stylers: [{
                saturation: 43
              }, {
                lightness: -11
              }, {
                hue: "#0088ff"
              }]
            }, {
              featureType: "road",
              elementType: "geometry.fill",
              stylers: [{
                hue: "#ff0000"
              }, {
                saturation: -100
              }, {
                lightness: 99
              }]
            }, {
              featureType: "road",
              elementType: "geometry.stroke",
              stylers: [{
                color: "#808080"
              }, {
                lightness: 54
              }]
            }, {
              featureType: "landscape.man_made",
              elementType: "geometry.fill",
              stylers: [{
                color: "#ece2d9"
              }]
            }, {
              featureType: "poi.park",
              elementType: "geometry.fill",
              stylers: [{
                color: "#ccdca1"
              }]
            }, {
              featureType: "road",
              elementType: "labels.text.fill",
              stylers: [{
                color: "#767676"
              }]
            }, {
              featureType: "road",
              elementType: "labels.text.stroke",
              stylers: [{
                color: "#ffffff"
              }]
            }, {
              featureType: "poi",
              stylers: [{
                visibility: "off"
              }]
            }, {
              featureType: "landscape.natural",
              elementType: "geometry.fill",
              stylers: [{
                visibility: "on"
              }, {
                color: "#b8cb93"
              }]
            }, {
              featureType: "poi.park",
              stylers: [{
                visibility: "on"
              }]
            }, {
              featureType: "poi.sports_complex",
              stylers: [{
                visibility: "on"
              }]
            }, {
              featureType: "poi.medical",
              stylers: [{
                visibility: "on"
              }]
            }, {
              featureType: "poi.business",
              stylers: [{
                visibility: "simplified"
              }]
            }]
          },
              s = new t.maps.Map(document.getElementById("map"), a),
              i = new t.maps.Marker({
            position: e,
            title: "Regular Map!"
          });
          i.setMap(s);
        });
      }
    },
        zs = Is,
        Es = (a("39eb"), Object(d["a"])(zs, $s, Ts, !1, null, null, null)),
        As = Es.exports,
        Ds = a("2f62"),
        Ms = a("ba38");

    s["default"].use(v.a, n.a);
    var Ls = new Ms["a"](s["default"].prototype.$http, {
      baseUrl: "http://kaviinternals.local/api/v1",
      tokenName: "access_token",
      loginUrl: "/login",
      registerUrl: "/register"
    }),
        Bs = {
      state: {
        isAuthenticated: null !== localStorage.getItem("vue-authenticate.vueauth_access_token")
      },
      getters: {
        isAuthenticated: function isAuthenticated(t) {
          return t.isAuthenticated;
        }
      },
      mutations: {
        isAuthenticated: function isAuthenticated(t, e) {
          t.isAuthenticated = e.isAuthenticated;
        }
      },
      actions: {
        login: function login(t, e) {
          return Ls.login(e.user, e.requestOptions).then(function (e) {
            t.commit("isAuthenticated", {
              isAuthenticated: Ls.isAuthenticated()
            }), Ii.push({
              name: "Home"
            });
          });
        },
        register: function register(t, e) {
          return Ls.register(e.user, e.requestOptions).then(function (e) {
            t.commit("isAuthenticated", {
              isAuthenticated: Ls.isAuthenticated()
            }), Ii.push({
              name: "Home"
            });
          });
        },
        logout: function logout(t, e) {
          return Ls.logout().then(function (e) {
            t.commit("isAuthenticated", {
              isAuthenticated: Ls.isAuthenticated()
            }), Ii.push({
              name: "Login"
            });
          });
        }
      }
    },
        Ns = (a("3d20"), {}),
        Rs = {},
        Vs = {
      success: function success(t, e) {
        t.commit, t.dispatch;
        this.$app.$notify({
          timeout: 2500,
          message: e,
          horizontalAlign: "right",
          verticalAlign: "top",
          icon: "add_alert",
          type: "success"
        });
      },
      error: function error(t, e) {
        t.commit, t.dispatch;
        this.$app.$notify({
          timeout: 2500,
          message: e,
          horizontalAlign: "right",
          verticalAlign: "top",
          icon: "add_alert",
          type: "warning"
        });
      }
    },
        Us = {},
        Hs = {
      namespaced: !0,
      state: Ns,
      getters: Us,
      actions: Vs,
      mutations: Rs
    },
        Fs = Hs,
        Ks = a("c010"),
        Ws = a.n(Ks),
        Ys = "http://kaviinternals.local/api/v1",
        qs = new Ws.a();

    function Gs() {
      return n.a.get("".concat(Ys, "/me")).then(function (t) {
        return {
          list: qs.deserialize(t.data),
          meta: t.data.meta
        };
      });
    }

    function Js(t) {
      var e = qs.serialize({
        stuff: t,
        includeNames: []
      }),
          a = {
        headers: {
          Accept: "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json"
        }
      };
      return n.a.patch("".concat(Ys, "/me"), e, a).then(function (t) {
        return qs.deserialize(t.data);
      });
    }

    var Xs = {
      get: Gs,
      update: Js
    },
        Qs = {
      me: null
    },
        Zs = {
      SET_RESOURCE: function SET_RESOURCE(t, e) {
        t.me = e;
      }
    },
        ti = {
      me: function me(t, e) {
        var a = t.commit;
        t.dispatch;
        return Xs.get(e).then(function (t) {
          a("SET_RESOURCE", t.list);
        });
      },
      update: function update(t, e) {
        var a = t.commit;
        t.dispatch;
        return Xs.update(e).then(function (t) {
          a("SET_RESOURCE", t);
        });
      }
    },
        ei = {
      me: function me(t) {
        return t.me;
      }
    },
        ai = {
      namespaced: !0,
      state: Qs,
      getters: ei,
      actions: ti,
      mutations: Zs
    },
        si = ai,
        ii = a("4328"),
        ni = a.n(ii),
        ri = "http://kaviinternals.local/api/v1",
        oi = new Ws.a();

    function li(t) {
      var e = {
        params: t,
        paramsSerializer: function paramsSerializer(t) {
          return ni.a.stringify(t, {
            encode: !1
          });
        }
      };
      return n.a.get("".concat(ri, "/users"), e).then(function (t) {
        return {
          list: oi.deserialize(t.data),
          meta: t.data.meta
        };
      });
    }

    function ci(t) {
      var e = {
        headers: {
          Accept: "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json"
        }
      };
      return n.a.get("".concat(ri, "/users/").concat(t), e).then(function (t) {
        var e = oi.deserialize(t.data);
        return delete e.links, e;
      });
    }

    function di(t) {
      var e = oi.serialize({
        stuff: t,
        includeNames: null
      }),
          a = {
        headers: {
          Accept: "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json"
        }
      };
      return n.a.post("".concat(ri, "/users"), e, a).then(function (t) {
        return oi.deserialize(t.data);
      });
    }

    function mi(t) {
      var e = oi.serialize({
        stuff: t,
        includeNames: []
      }),
          a = {
        headers: {
          Accept: "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json"
        }
      };
      return n.a.patch("".concat(ri, "/users/").concat(t.id), e, a).then(function (t) {
        return oi.deserialize(t.data);
      });
    }

    function ui(t) {
      var e = {
        headers: {
          Accept: "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json"
        }
      };
      return n.a["delete"]("".concat(ri, "/users/").concat(t), e);
    }

    function pi(t, e) {
      var a = new FormData();
      return a.append("attachment", e), n.a.post("".concat(ri, "/uploads/users/").concat(t.id, "/profile-image"), a).then(function (t) {
        return t.data.url;
      });
    }

    var hi = {
      list: li,
      get: ci,
      add: di,
      update: mi,
      destroy: ui,
      upload: pi
    },
        fi = {
      list: {},
      user: {},
      meta: {}
    },
        vi = {
      SET_LIST: function SET_LIST(t, e) {
        t.list = e;
      },
      SET_RESOURCE: function SET_RESOURCE(t, e) {
        t.user = e;
      },
      SET_META: function SET_META(t, e) {
        t.meta = e;
      }
    },
        bi = {
      list: function list(t, e) {
        var a = t.commit;
        t.dispatch;
        return hi.list(e).then(function (t) {
          var e = t.list,
              s = t.meta;
          a("SET_LIST", e), a("SET_META", s);
        });
      },
      get: function get(t, e) {
        var a = t.commit;
        t.dispatch;
        return hi.get(e).then(function (t) {
          a("SET_RESOURCE", t);
        });
      },
      add: function add(t, e) {
        var a = t.commit;
        t.dispatch;
        return hi.add(e).then(function (t) {
          a("SET_RESOURCE", t);
        });
      },
      update: function update(t, e) {
        var a = t.commit;
        t.dispatch;
        return hi.update(e).then(function (t) {
          a("SET_RESOURCE", t);
        });
      },
      destroy: function destroy(t, e) {
        t.commit, t.dispatch;
        return hi.destroy(e);
      }
    },
        gi = {
      list: function list(t) {
        return t.list;
      },
      listTotal: function listTotal(t) {
        return t.meta.page.total;
      },
      user: function user(t) {
        return t.user;
      }
    },
        _i = {
      namespaced: !0,
      state: fi,
      getters: gi,
      actions: bi,
      mutations: vi
    },
        Ci = _i;
    s["default"].use(Ds["a"]);
    var yi = new Ds["a"].Store({
      modules: {
        auth: Bs,
        alerts: Fs,
        profile: si,
        users: Ci
      }
    });

    function wi(t) {
      var e = t.next,
          a = t.router;
      return yi.getters.isAuthenticated ? e() : a.push({
        name: "Login"
      });
    }

    function ki(t) {
      var e = t.next,
          a = t.router;
      return yi.getters.isAuthenticated ? a.push({
        name: "Home"
      }) : e();
    }

    var xi = {
      path: "/components",
      component: te,
      redirect: "/components/notification",
      name: "Components",
      children: [{
        path: "table",
        name: "Table",
        components: {
          "default": Ss
        },
        meta: {
          middleware: wi
        }
      }, {
        path: "typography",
        name: "Typography",
        components: {
          "default": _s
        },
        meta: {
          middleware: wi
        }
      }, {
        path: "icons",
        name: "Icons",
        components: {
          "default": ps
        },
        meta: {
          middleware: wi
        }
      }, {
        path: "maps",
        name: "Maps",
        meta: {
          hideContent: !0,
          hideFooter: !0,
          navbarAbsolute: !0,
          middleware: wi
        },
        components: {
          "default": As
        }
      }, {
        path: "notifications",
        name: "Notifications",
        components: {
          "default": os
        },
        meta: {
          middleware: wi
        }
      }, {
        path: "rtl",
        name: "وحة القيادة",
        meta: {
          rtlActive: !0,
          middleware: wi
        },
        components: {
          "default": Ha
        }
      }]
    },
        Si = {
      path: "/examples",
      component: te,
      name: "Examples",
      children: [{
        path: "user-profile",
        name: "User Profile",
        components: {
          "default": Ia
        },
        meta: {
          middleware: wi
        }
      }, {
        path: "user-management/list-users",
        name: "List Users",
        components: {
          "default": La
        },
        meta: {
          middleware: wi
        }
      }]
    },
        $i = {
      path: "/",
      component: re,
      name: "Authentication",
      children: [{
        path: "/login",
        name: "Login",
        component: Ga,
        meta: {
          middleware: ki
        }
      }, {
        path: "/register",
        name: "Register",
        component: es,
        meta: {
          middleware: ki
        }
      }]
    },
        Ti = [{
      path: "/",
      redirect: "/dashboard",
      name: "Home"
    }, {
      path: "/",
      component: te,
      meta: {
        middleware: wi
      },
      children: [{
        path: "dashboard",
        name: "Dashboard",
        components: {
          "default": na
        },
        meta: {
          middleware: wi
        }
      }]
    }, xi, Si, $i],
        Pi = Ti;
    s["default"].use(dt["a"]);
    var ji = new dt["a"]({
      mode: "history",
      routes: Pi,
      scrollBehavior: function scrollBehavior(t) {
        return t.hash ? {
          selector: t.hash
        } : {
          x: 0,
          y: 0
        };
      },
      linkExactActiveClass: "nav-item active"
    });

    function Oi(t, e, a) {
      var s = e[a];
      return s ? function () {
        t.next.apply(t, arguments);
        var i = Oi(t, e, a + 1);
        s(Object(ct["a"])(Object(ct["a"])({}, t), {}, {
          next: i
        }));
      } : t.next;
    }

    ji.beforeEach(function (t, e, a) {
      if (t.meta.middleware) {
        var s = Array.isArray(t.meta.middleware) ? t.meta.middleware : [t.meta.middleware],
            i = {
          from: e,
          next: a,
          to: t,
          router: ji
        },
            n = Oi(i, s, 1);
        return s[0](Object(ct["a"])(Object(ct["a"])({}, i), {}, {
          next: n
        }));
      }

      return a();
    });
    var Ii = ji;
    s["default"].use(lt), s["default"].use(v.a, n.a), s["default"].prototype.$Chartist = h.a;
    var zi = new s["default"]({
      router: Ii,
      store: yi,
      el: "#app",
      render: function render(t) {
        return t(u);
      }
    });
    yi.$app = zi;
  },
  "57e7": function e7(t, e, a) {
    "use strict";

    var s = a("0a95"),
        i = a.n(s);
    i.a;
  },
  6683: function _(t, e, a) {},
  "6ae5": function ae5(t, e, a) {
    "use strict";

    var s = a("b5b2"),
        i = a.n(s);
    i.a;
  },
  "7f3c": function f3c(t, e, a) {},
  8075: function _(t, e, a) {
    "use strict";

    var s = a("6683"),
        i = a.n(s);
    i.a;
  },
  "81ed": function ed(t, e, a) {},
  8475: function _(t, e, a) {},
  "951c": function c(t, e, a) {
    "use strict";

    var s = a("338e"),
        i = a.n(s);
    i.a;
  },
  "9e17": function e17(t, e, a) {},
  "9e9e": function e9e(t, e, a) {},
  a020: function a020(t, e, a) {
    "use strict";

    var s = a("81ed"),
        i = a.n(s);
    i.a;
  },
  b5b2: function b5b2(t, e, a) {},
  c6e3: function c6e3(t, e, a) {},
  cd1f: function cd1f(t, e, a) {},
  e87a: function e87a(t, e, a) {
    "use strict";

    var s = a("7f3c"),
        i = a.n(s);
    i.a;
  },
  f2eb: function f2eb(t, e, a) {
    "use strict";

    var s = a("52d0"),
        i = a.n(s);
    i.a;
  },
  fc63: function fc63(t, e, a) {
    "use strict";

    var s = a("9e9e"),
        i = a.n(s);
    i.a;
  },
  fce4: function fce4(t, e, a) {
    "use strict";

    var s = a("8475"),
        i = a.n(s);
    i.a;
  }
});

/***/ }),

/***/ 0:
/*!***********************************!*\
  !*** multi ./resources/js/app.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\kavi-internal-api\resources\js\app.js */"./resources/js/app.js");


/***/ })

/******/ });